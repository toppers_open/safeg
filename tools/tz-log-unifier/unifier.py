# -*- coding: utf-8 -*-
import re
import string

#def main():
	#total = []
	#fd = open("nontrust.log")
	## 1. create a list of [time,string]
	#for line in fd:
		#match = re.match('\[(\d*)\](.*)', line)
		#if match:
			#groups = list(match.groups())
			#total.append(groups)
	## 2. create a list of [time,string]
	#prev = 0
	#for entry in total:
		#match = re.match(' enter to cyclic handler', entry[1])
		#if match:
			#print int(entry[0]) - prev
			#prev = int(entry[0])

#def main():
	#total = []
	#files = [open("trust.log"),open("nontrust.log")]
	## 1. create a list of [time,string]
	#for i, fd in enumerate(files):
		#for line in fd:
			#match = re.match('\[(\d*)\](.*)', line)
			#if match:
				#groups = list(match.groups())
				#groups.append(str(i+1))
				#total.append(groups)
	## 2. sort the list by time
	#s = sorted(total, key=lambda value: value[0], reverse=False)
	## 5. write to a combined log
	#out = open("combined.log","w")
	#for entry in s:
		#out.write("["+entry[0]+"]:["+entry[2]+"]:"+entry[1]+"\n")

#def main():
	#o = open("combined-rename.log","w")
	#data = open("combined.log").read()
	#data = re.sub("task 10","NT_TASK1",data)
	#data = re.sub("task 11","NT_TASK2",data)
	#data = re.sub("task 12","NT_TASK3",data)
	#data = re.sub("task 13","TRUST",data)
	#data = re.sub("task 1","MAIN_TASK",data)
	#data = re.sub("task 2","TASK1",data)
	#data = re.sub("task 3","TASK2",data)
	#data = re.sub("task 4","TASK3",data)
	#data = re.sub("task 5","LMANG",data)
	#data = re.sub("task 6","LTASK0",data)
	#data = re.sub("task 7","LTASK1",data)
	#data = re.sub("task 8","BTASK",data)
	#data = re.sub("task 9","NT_MAIN_TASK",data)
	#o.write(data)
	#o.close()

def main():
	total = []
	files = [open("trust.log"),open("nontrust.log")]
	# 1. create a list of [time,string]
	for i, fd in enumerate(files):
		for line in fd:
			match = re.match('\[(\d*)\](.*)', line)
			if match:
				groups = list(match.groups())
				groups.append(str(i+1))
				total.append(groups)
	# 2. sort the list by time
	s = sorted(total, key=lambda value: value[0], reverse=False)
	# 3. insert "[2] leave from TRUST" after "[1] dispatch to LTASK"
	TRUST_ID = "13"
	LTASK_RANGE = range(6,9)
	NT_RANGE = range(9,13)
	nt_running = 9
	total = []
	for entry in s:
		total.append(entry)
		match = re.match(' dispatch to task (\d*).', entry[1])
		if match:
			task = int(match.groups()[0])
			if task in LTASK_RANGE:
				total.append([entry[0], " dispatch from task "+TRUST_ID+".", "2"])
				total.append([entry[0], " dispatch to task "+str(nt_running)+".", "2"])
			elif task in NT_RANGE:
				nt_running = task
	# 4. detect NT -> T edges and insert a "[2] enter to TRUST"
	nt_running = 9
	prev_proc_id = 1
	s = total
	total = []
	for entry in s:
		match = re.match(' dispatch to task (\d*).', entry[1])
		if match:
			task = int(match.groups()[0])
			if task in NT_RANGE:
				nt_running = task
		if (int(entry[2]) == 1) and (prev_proc_id == 2): # NT -> T
			total.append([entry[0], " dispatch from task "+str(nt_running)+".", "2"])
			total.append([entry[0], " dispatch to task "+TRUST_ID+".", "2"])
		prev_proc_id = int(entry[2])
		total.append(entry)
	# 5. fix dispatches that were interrupted by TRUST
	for i, entry in enumerate(total):
		if entry[2] == "1":
			continue
		match = re.match(' dispatch to task (\d*).', entry[1])
		if match:
			match = re.match(' dispatch to task (\d*).', total[i-1][1])
			if match:
				del total[i-1]
		else:
			match = re.match(' dispatch from task (\d*).', entry[1])
			if match:
				match = re.match(' dispatch from task (\d*).', total[i-1][1])
				if match:
					del total[i-1]
	# 5. write to a combined log
	out = open("combined.log","w")
	for entry in total:
		out.write("["+entry[0]+"]:["+entry[2]+"]:"+entry[1]+"\n")

if __name__ == '__main__':
	main()




