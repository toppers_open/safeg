#include <stdio.h>   /* printf */
#include <string.h>  /* strncpy */
#include <assert.h>  /* assert */
#include <stdint.h>  /* uint32_t */
#include <stdbool.h> /* true, false, bool */
#include <stdlib.h>  /* malloc */
#include <stdarg.h>  /* va_list */
#include <ctype.h>   /* toupper */

#include "linux_list.h" /* list_add_tail */

#define MAX_CHARS 256
#define MAX_PARAM_SIZE_WORDS 4

/*
 * RTOS or GPOS
 */
#define NUM_OS 2

enum os_t {RTOS = 0, GPOS = 1};
enum channel_type {CTYPE_FIFO = 0, CTYPE_SMEM = 1};

static char *config_filename[] = {"rtos-dualoscom_config.h", "gpos-dualoscom_config.h"};

/*
 * FILTERS
 */
#define DUALOSCOM_FILTER(FILTER_NAME, filter_function, in_rtos, in_gpos) \
    assert(add_dualoscom_filter(#FILTER_NAME, #filter_function, (in_rtos), (in_gpos)) == 0);

struct filter_t {
    uint32_t            id[NUM_OS];
    char                filter_name[MAX_CHARS];
    char                function_name[MAX_CHARS];
    bool                is_valid[NUM_OS];
    struct list_head    filter_list;
};

static struct filter_t the_filter_list;
static uint32_t the_num_of_filters[NUM_OS] = {0, 0};

int add_dualoscom_filter(const char *name, const char *func, bool in_rtos, bool in_gpos)
{
    struct filter_t *filter;

    filter = (struct filter_t *)malloc(sizeof(struct filter_t));
    assert(filter != NULL);

    strncpy(filter->filter_name, name, MAX_CHARS);
    strncpy(filter->function_name, func, MAX_CHARS);
    filter->is_valid[RTOS] = in_rtos;
    filter->is_valid[GPOS] = in_gpos;

    if (filter->is_valid[RTOS]) filter->id[RTOS] = the_num_of_filters[RTOS]++;
    if (filter->is_valid[GPOS]) filter->id[GPOS] = the_num_of_filters[GPOS]++;

    list_add_tail(&filter->filter_list, &the_filter_list.filter_list);

    return 0;
}

/*
 * CHANNELS
 */
#define DUALOSCOM_FIFO(CHANNEL_NAME, num_blocks, block_size, rtos_mutex, gpos_mutex, rtos_default_filter, gpos_default_filter) \
    assert(add_dualoscom_channel(#CHANNEL_NAME, (num_blocks), (block_size), (rtos_mutex), (gpos_mutex), #rtos_default_filter, #gpos_default_filter, CTYPE_FIFO) == 0);

#define DUALOSCOM_SMEM(CHANNEL_NAME, num_blocks, block_size) \
    assert(add_dualoscom_channel(#CHANNEL_NAME, (num_blocks), (block_size), false, false, "NULL_FILTER", "NULL_FILTER", CTYPE_SMEM) == 0);

struct channel_t {
    uint32_t            id;
    char                channel_name[MAX_CHARS];
    uint32_t            num_blocks;
    uint32_t            block_size;
    bool                mutexes[NUM_OS];
    char                default_filter[NUM_OS][MAX_CHARS];
    uint32_t            memory_size;
    struct list_head    channel_list;
    enum channel_type   channel_type;
};

static struct channel_t the_channel_list;
static uint32_t the_num_of_channels = 0;

int add_dualoscom_channel(const char *name,
                          uint32_t num_blocks,
                          uint32_t block_size,
                          bool rtos_mutex,
                          bool gpos_mutex,
                          const char *rtos_default_filter,
                          const char *gpos_default_filter,
                          enum channel_type channel_type)
{
    struct channel_t *channel;

    channel = (struct channel_t *)malloc(sizeof(struct channel_t));
    assert(channel != NULL);

    channel->id         = the_num_of_channels++;
    channel->num_blocks = num_blocks;
    channel->block_size = block_size;
    channel->mutexes[RTOS] = rtos_mutex;
    channel->mutexes[GPOS] = gpos_mutex;
    channel->channel_type = channel_type;
    strncpy(channel->channel_name, name, MAX_CHARS);
    strncpy(channel->default_filter[RTOS], rtos_default_filter, MAX_CHARS);
    strncpy(channel->default_filter[GPOS], gpos_default_filter, MAX_CHARS);

    // see dualoscom-shmem.pdf
    channel->memory_size =   8 // RTOS2GPOS FIFO write and read cursor
                           + 4 // RTOS2GPOS Filter ID
                           + 8 // GPOS2RTOS FIFO write and read cursor
                           + 4 // GPOS2RTOS Filter ID
                           + 4 * (channel->num_blocks + 1) // GPOS2RTOS FIFO queue
                           + 4 * (channel->num_blocks + 1) // RTOS2GPOS FIFO queue
                           + channel->num_blocks * (channel->block_size*4 + 4); // locks per block

    list_add_tail(&channel->channel_list, &the_channel_list.channel_list);

    return 0;
}

/*
 * GROUPS
 */
#define DUALOSCOM_CHANNELS_GROUP(GROUP_NAME, num_channels, ...) \
    assert(add_dualoscom_group(#GROUP_NAME, (num_channels), #__VA_ARGS__) == 0);

struct group_t {
    uint32_t            id;
    char                group_name[MAX_CHARS];
    uint32_t            num_channels;
    char                channels_names[MAX_CHARS];
    struct list_head    group_list;
};

static struct group_t the_group_list;
static uint32_t the_num_of_groups = 0;

int add_dualoscom_group(const char *name, uint32_t num_channels, ...)
{
    struct group_t *group;
    va_list ap;
    char *channel_names;

    group = (struct group_t *)malloc(sizeof(struct group_t));
    assert(group != NULL);

    group->id = the_num_of_groups++;
    strncpy(group->group_name, name, MAX_CHARS);
    group->num_channels = num_channels;

    va_start(ap, num_channels);
    channel_names = va_arg(ap, char *);
    va_end(ap);

    strncpy(group->channels_names, channel_names, MAX_CHARS);

    list_add_tail(&group->group_list, &the_group_list.group_list);

    return 0;
}

/*
 * RPCs
 */
#define RTOS2GPOS 4141
#define GPOS2RTOS 5959
#define MAX_PARAMS 32

#define DUALOSCOM_RPC(function, direction, mutex, parameters) \
    assert(add_dualoscom_rpc(#function, (direction), (mutex), (parameters), CTYPE_FIFO) == 0);

struct rpc_param_t {
    char name[MAX_CHARS];
    bool is_out;
    char type[MAX_CHARS];
};

struct rpc_t {
    uint32_t            id;
    char                function[MAX_CHARS];
    uint32_t            direction;
    bool                mutex;
    uint32_t            num_params;
    struct rpc_param_t  params[MAX_PARAMS];
    char                rpc_channel_name[MAX_CHARS];
    uint32_t            rpc_block_size;
    struct list_head    rpc_list;
};

static struct rpc_t the_rpc_list;
static uint32_t the_num_of_rpcs = 0;

void uppercase(char *string)
{
    while(*string != '\0' ) {
        *string = toupper((unsigned char)*string);
        string++;
    }
}

int add_dualoscom_rpc(const char *function, uint32_t direction, bool mutex, char parameters[])
{
    struct rpc_t *rpc;
    char *par;
    char *params;
    bool rtos_mutex, gpos_mutex;

    // Allocate a new RPC
    rpc = (struct rpc_t *)malloc(sizeof(struct rpc_t));
    assert(rpc != NULL);

    // set the basic information
    strncpy(rpc->function, function, MAX_CHARS);
    rpc->id        = the_num_of_rpcs++;
    rpc->direction = direction;
    rpc->mutex     = mutex;

    // parse the rpc parameters
    params = (char *)malloc(sizeof(char)*strlen(parameters));
    assert(params != NULL);
    strncpy(params, parameters, sizeof(char)*strlen(parameters));

    rpc->num_params = 0;
    par = strtok(params, " ,:");
    while(par != NULL) {
        // extract parameter name
        strncpy(rpc->params[rpc->num_params].name, par, MAX_CHARS);
        par = strtok(NULL, " ,:");
        assert(par != NULL);
        // extract parameter direction
        rpc->params[rpc->num_params].is_out = (strncmp("out", par, 3) == 0);
        par = strtok(NULL, " ,:");
        assert(par != NULL);
        // extract parameter type (TODO: support params such "unsigned char")
        strncpy(rpc->params[rpc->num_params].type, par, MAX_CHARS);
        par = strtok(NULL, " ,:");
        rpc->num_params++;
    }

    // add rpc to the list
    list_add_tail(&rpc->rpc_list, &the_rpc_list.rpc_list);

    // prepare the channel name (RPC_FUNCTION)
    snprintf(rpc->rpc_channel_name, MAX_CHARS, "RPC_%s", rpc->function);
    uppercase(rpc->rpc_channel_name);

    // calculate the necessary block size in words (TODO: provide a more accurate way)
    rpc->rpc_block_size = rpc->num_params * MAX_PARAM_SIZE_WORDS;

    // decide which mutexes are needed
    rtos_mutex = (rpc->direction == RTOS2GPOS) && rpc->mutex;
    gpos_mutex = (rpc->direction == GPOS2RTOS) && rpc->mutex;

    // add a channel for the RPC
    add_dualoscom_channel(rpc->rpc_channel_name, /* channel name */
                          1,                     /* num_blocks */
                          rpc->rpc_block_size,   /* block size (words) */
                          rtos_mutex,            /* put a mutex on the RTOS */
                          gpos_mutex,            /* put a mutex on the GPOS */
                          "NULL_FILTER",         /* rtos_default_filter */
                          "NULL_FILTER",        /* gpos_default_filter */
                          CTYPE_FIFO);
    // free the memory allocated for params
    free(params);

    return 0;
}

/*
 * CONFIG INIT
 */
void config_initialize(void)
{
    INIT_LIST_HEAD(&the_filter_list.filter_list);
    INIT_LIST_HEAD(&the_channel_list.channel_list);
    INIT_LIST_HEAD(&the_group_list.group_list);
    INIT_LIST_HEAD(&the_rpc_list.rpc_list);
#include CONFIG_FILE
}

/*
 * CONFIG GENERATE
 */
void config_generate(enum os_t os)
{
    struct filter_t *filter;
    struct channel_t *channel;
    struct group_t *group;
    struct rpc_t *rpc;
    FILE   *fp;
    uint32_t offset;
    bool associated, match;
    struct rpc_param_t *par;
    int i;

    fp = fopen(config_filename[os], "w");
    assert(fp != NULL);

    fprintf(fp,"#ifndef DUALOSCOM_CONFIG\n");
    fprintf(fp,"#define DUALOSCOM_CONFIG\n");

    fprintf(fp,"\n#include \"dualoscom_hw.h\"\n");
    fprintf(fp,"\n#include \"dualoscom.h\"\n");

    /*
     * Filters
     */

    fprintf(fp,"\n/* FILTERS */\n");
    fprintf(fp,"#define NULL_FILTER 0xFFFFFFFF\n");

    // set number of filters (if there are 0 filters, it is not defined)
    if (the_num_of_filters[RTOS] > 0)
        fprintf(fp,"#define DUALOSCOM_NUM_RTOS2GPOS_FILTERS %u\n", the_num_of_filters[RTOS]);

    if (the_num_of_filters[GPOS] > 0)
        fprintf(fp,"#define DUALOSCOM_NUM_GPOS2RTOS_FILTERS %u\n", the_num_of_filters[GPOS]);

    // set the filter ids (for the other os)
    if (the_num_of_filters[(os+1)%NUM_OS] > 0) {
        list_for_each_entry(filter, &the_filter_list.filter_list, filter_list) {
            if (!filter->is_valid[(os+1)%NUM_OS]) continue;
            fprintf(fp,"#define %s %u\n", filter->filter_name, filter->id[(os+1)%NUM_OS]);
        }
    }

    if (the_num_of_filters[os] > 0) {
        // define filter function prototypes if they are valid in the current OS
        list_for_each_entry(filter, &the_filter_list.filter_list, filter_list) {
            if (!filter->is_valid[os]) continue;
            fprintf(fp,"extern %s %s(void *buffer, uint32_t size);\n", (os==RTOS)?"bool_t":"bool",filter->function_name);
        }

        // set the names of the filters
        fprintf(fp,"#define DUALOSCOM_FILTERS {");
        list_for_each_entry(filter, &the_filter_list.filter_list, filter_list) {
            if (!filter->is_valid[os]) continue;
            if (filter->id[os] != (the_num_of_filters[os] - 1))
                fprintf(fp,"%s, ", filter->function_name);
            else
                fprintf(fp,"%s}\n", filter->function_name);
        }
    }

    /*
     * Channels
     */

    if (the_num_of_channels > 0) {
        fprintf(fp,"\n/* CHANNELS */\n");

        // set number of channels (if there are 0 channels, it is not defined)
        fprintf(fp,"#define DUALOSCOM_NUM_CHANNELS %u\n", the_num_of_channels);

        // set the ids of the channels
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            fprintf(fp,"#define %s %u\n", channel->channel_name, channel->id);
        }
        // set the number of blocks
        fprintf(fp,"#define DUALOSCOM_NUM_BLOCKS {");
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,"%u, ", channel->num_blocks);
            else
                fprintf(fp,"%u}\n", channel->num_blocks);
        }
        // set the block sizes
        fprintf(fp,"#define DUALOSCOM_BLOCK_SIZES {");
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,"%u, ", channel->block_size);
            else
                fprintf(fp,"%u}\n", channel->block_size);
        }
        // set mutex protection booleans
        fprintf(fp,"#define DUALOSCOM_PROTECTED {");
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,"%s, ", channel->mutexes[os]?"true":"false");
            else
                fprintf(fp,"%s}\n",  channel->mutexes[os]?"true":"false");
        }
        // set channel type
        fprintf(fp,"#define DUALOSCOM_CHANNEL_TYPE {");
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,"%d,", channel->channel_type);
            else
                fprintf(fp,"%d}\n", channel->channel_type);
        }
        // set semaphores
        if (os == RTOS) {
            fprintf(fp,"#define DUALOSCOM_SEMAPHORES {");
            list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
                if (channel->id != (the_num_of_channels - 1))
                    fprintf(fp,"DUALOSCOM_SEM_CHANNEL_%u, ", channel->id);
                else
                    fprintf(fp,"DUALOSCOM_SEM_CHANNEL_%u}\n", channel->id);
            }
        }
        // set default filters
        if (os == RTOS)
            fprintf(fp,"#define DUALOSCOM_DEFAULT_GPOS2RTOS_FILTERS {");
        else
            fprintf(fp,"#define DUALOSCOM_DEFAULT_RTOS2GPOS_FILTERS {");

        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            // make sure that the default filter exists in the OS
            match = false;
            if(strncmp(channel->default_filter[os], "NULL_FILTER", MAX_CHARS) == 0) {
                match = true;
            } else {
                list_for_each_entry(filter, &the_filter_list.filter_list, filter_list) {
                    if(strncmp(channel->default_filter[os], filter->filter_name, MAX_CHARS) == 0) {
                        assert(filter->is_valid[os]);
                        match = true;
                    }
                }
            }
            assert(match);

            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,"%s, ", channel->default_filter[os]);
            else
                fprintf(fp,"%s}\n",  channel->default_filter[os]);
        }
    }

    /*
     * Groups
     */

    if (the_num_of_groups > 0) {
        fprintf(fp,"\n/* GROUPS */\n");

        // set number of groups (if there are 0 groups, it is not defined)
        fprintf(fp,"#define DUALOSCOM_NUM_GROUPS %u\n", the_num_of_groups);

        // set the ids of the groups
        list_for_each_entry(group, &the_group_list.group_list, group_list) {
            fprintf(fp,"#define %s %u\n", group->group_name, group->id);
        }
        // set which channels are associated (true) and which are not (false)
        fprintf(fp,"#define DUALOSCOM_CHANNEL2GROUPS {");
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            fprintf(fp,"{");
            list_for_each_entry(group, &the_group_list.group_list, group_list) {
                associated = (strstr(group->channels_names, channel->channel_name) != NULL);
                if (group->id != (the_num_of_groups - 1))
                    fprintf(fp,"%s, ", associated?"true":"false");
                else
                    fprintf(fp,"%s}", associated?"true":"false");
            }
            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,", ");
            else
                fprintf(fp,"}\n");
        }

        // set semaphores
        if (os == RTOS) {
            fprintf(fp,"#define DUALOSCOM_GROUP_SEMAPHORES {");
            list_for_each_entry(group, &the_group_list.group_list, group_list) {
                if (group->id != (the_num_of_groups - 1))
                    fprintf(fp,"DUALOSCOM_SEM_GROUP_%u, ", group->id);
                else
                    fprintf(fp,"DUALOSCOM_SEM_GROUP_%u}\n", group->id);
            }
        }
    }

    /*
     * RPCs
     */

    if (the_num_of_rpcs > 0) {
        fprintf(fp,"\n/* RPCs */\n");

        list_for_each_entry(rpc, &the_rpc_list.rpc_list, rpc_list) {
            // define type structure
            fprintf(fp,"typedef struct {\n");
            for(i=0; i<rpc->num_params; i++) {
                par = &rpc->params[i];
                fprintf(fp,"    %s %s;\n", par->type, par->name);
            }
            fprintf(fp,"} dualoscom_%s_params_t;\n\n", rpc->function);

            // define the rpc stubs
            if (((os == RTOS) && (rpc->direction == RTOS2GPOS)) ||
                ((os == GPOS) && (rpc->direction == GPOS2RTOS))) {
                // client stub
                fprintf(fp,"static inline void %s(", rpc->function);
                for(i=0; i<rpc->num_params; i++) {
                    par = &rpc->params[i];
                    if (i == rpc->num_params-1) {
                        fprintf(fp,"%s %s %s)\n{\n",
                                par->type, par->is_out?"*":"", par->name);
                    } else {
                        fprintf(fp,"%s %s %s, ",
                                par->type, par->is_out?"*":"", par->name);
                    }
                }
                fprintf(fp,"    dualoscom_block_id_t block_id;\n");
                fprintf(fp,"    uint32_t size;\n");
                fprintf(fp,"    dualoscom_%s_params_t *params;\n\n", rpc->function);

                fprintf(fp,"    dualoscom_block_alloc(%s, &block_id);\n", rpc->rpc_channel_name);
                fprintf(fp,"    dualoscom_block_getbuffer(block_id, (void **)&params, &size);\n");
                fprintf(fp,"    assert(sizeof(dualoscom_%s_params_t) < (size*4));\n", rpc->function);

                for(i=0; i<rpc->num_params; i++) {
                    par = &rpc->params[i];
                    if (par->is_out) continue;
                    fprintf(fp,"    params->%s = %s;\n", par->name, par->name);
                }

                fprintf(fp,"    dualoscom_block_enqueue(block_id);\n");
                fprintf(fp,"    dualoscom_channel_event_send(%s);\n", rpc->rpc_channel_name);
                fprintf(fp,"    dualoscom_channel_event_wait(%s, 0);\n", rpc->rpc_channel_name);

                for(i=0; i<rpc->num_params; i++) {
                    par = &rpc->params[i];
                    if (!par->is_out) continue;
                    fprintf(fp,"    *%s = params->%s;\n", par->name, par->name);
                }

                fprintf(fp,"    dualoscom_block_free(block_id);\n}\n");
            } else {
                // server stub
                fprintf(fp,"extern void %s(", rpc->function);
                for(i=0; i<rpc->num_params; i++) {
                    par = &rpc->params[i];
                    if (i == rpc->num_params-1) {
                        fprintf(fp,"%s %s %s);\n\n",
                                par->type, par->is_out?"*":"", par->name);
                    } else {
                        fprintf(fp,"%s %s %s, ",
                                par->type, par->is_out?"*":"", par->name);
                    }
                }

                fprintf(fp,"static inline void server_%s(void)\n{\n", rpc->function);

                fprintf(fp,"    dualoscom_block_id_t block_id;\n");
                fprintf(fp,"    uint32_t size;\n");
                fprintf(fp,"    dualoscom_%s_params_t *params;\n\n", rpc->function);

                fprintf(fp,"    dualoscom_channel_event_wait(%s, 0);\n", rpc->rpc_channel_name);
                fprintf(fp,"    dualoscom_block_dequeue(%s, &block_id);\n", rpc->rpc_channel_name);
                fprintf(fp,"    dualoscom_block_getbuffer(block_id, (void **)&params, &size);\n");
                fprintf(fp,"    assert(sizeof(dualoscom_%s_params_t) < (size*4));\n", rpc->function);

                fprintf(fp,"    %s(", rpc->function);
                for(i=0; i<rpc->num_params; i++) {
                    par = &rpc->params[i];
                    if (i == rpc->num_params-1) {
                        fprintf(fp,"%sparams->%s);\n", par->is_out?"&":"", par->name);
                    } else {
                        fprintf(fp,"%sparams->%s, ", par->is_out?"&":"", par->name);
                    }
                }

                fprintf(fp,"    dualoscom_channel_event_send(%s);\n}\n", rpc->rpc_channel_name);
            }
        }
    }

    /* Shared Memory */

    fprintf(fp,"\n/* SHARED MEMORY */\n");

    if (the_num_of_channels > 0) {
        fprintf(fp,"#define DUALOSCOM_SHMEM_OFFSETS {");
        offset = 0;
        list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
            if (channel->id != (the_num_of_channels - 1))
                fprintf(fp,"%u, ", offset);
            else
                fprintf(fp,"%u}\n", offset);
            offset = offset + channel->memory_size;
        }
        fprintf(fp,"#define DUALOSCOM_SHMEM_PENDING_EVENTS %u\n", offset);
        fprintf(fp,"#define DUALOSCOM_SHMEM_SIZE %u\n", offset + 8 * the_num_of_channels);
    }

    fprintf(fp,"\n#endif\n");
    fclose(fp);

    /*
     * Create ASP cfg file with semaphores etc.
     */
    if (os == RTOS) {
        fp = fopen("rtos-dualoscom.cfg", "w");
        assert(fp != NULL);

        fprintf(fp,"#include \"rtos-dualoscom_config.h\"\n\n");

        fprintf(fp,"DEF_INH(NT_INTNO_SOFTIRQ, { TA_NULL, dualoscom_handler });\n");
        fprintf(fp,"CFG_INT(NT_INTNO_SOFTIRQ, { 0, NT_INT_PRIORITY });\n\n");

        fprintf(fp,"CRE_SEM(DUALOSCOM_SEM_INIT, { TA_NULL, 0, 1});\n");
        if (the_num_of_channels > 0) {
            list_for_each_entry(channel, &the_channel_list.channel_list, channel_list) {
                fprintf(fp,"CRE_SEM(DUALOSCOM_SEM_CHANNEL_%u, { TA_NULL, 0, 1});\n", channel->id);
            }
        }

        if (the_num_of_groups > 0) {
            list_for_each_entry(group, &the_group_list.group_list, group_list) {
                fprintf(fp,"CRE_SEM(DUALOSCOM_SEM_GROUP_%u, { TA_NULL, 0, 1});\n", group->id);
            }
        }
        fclose(fp);
    }
}

/*
 * MAIN
 */
int main()
{
    config_initialize();
    config_generate(RTOS);
    config_generate(GPOS);
    return 0;
}
