
                Dual-OS Communications Configurator
                ===================================
                        by Daniel Sangorrin
                   (daniel.sangorrin@gmail.com)

This directory contains a configurator application for SafeG dual-OS
communications. The program is built and executed as follows:

    $ ./configure.sh

The program requires a configuration file named "config.txt" on the same
directory. The syntaxis of the "config.txt" configuration file is described in
the document "safeg-dualoscom-manual.pdf". You can use the file
"config.txt_sample" as an example:

DUALOSCOM_FILTER(MY_FILTER0, func_my_filter0, true, false)
DUALOSCOM_CHANNEL(MY_CHANNEL0, 10, 64, true, false, MY_FILTER0, NULL)
DUALOSCOM_FILTER(MY_FILTER1, func_my_filter1, false, true)
DUALOSCOM_CHANNEL(MY_CHANNEL1, 15, 128, false, true, MY_FILTER1, NULL)
DUALOSCOM_FILTER(MY_FILTER2, func_my_filter2, true, true)
DUALOSCOM_CHANNEL(MY_CHANNEL2, 20, 256, false, false, MY_FILTER2, NULL)
DUALOSCOM_CHANNELS_GROUP(MY_GROUP0, 1, MY_CHANNEL0)
DUALOSCOM_CHANNELS_GROUP(MY_GROUP1, 2, MY_CHANNEL0, MY_CHANNEL1)
DUALOSCOM_CHANNELS_GROUP(MY_GROUP2, 3, MY_CHANNEL0, MY_CHANNEL1, MY_CHANNEL2)

The configurator generates the following files:

    rtos-dualoscom.cfg
    rtos-dualoscom_config.h
    gpos-dualoscom_config.h

Currently, you must move those files manually to the corresponding OS.
For example:

    $ mv rtos-dualoscom.cfg $(ASPDIR)/obj/obj_safeg_dualoscom_config/trust/dualoscom.cfg
    $ mv rtos-dualoscom_config.h $(ASPDIR)/obj/obj_safeg_dualoscom_config/trust/dualoscom_config.h
    $ mv gpos-dualoscom_config.h $(LINUXDIR)/safeg/dualoscom_config.h

After that you have to recompile your RTOS application or GPOS kernel (only
code that depends on these files will be recompiled actually).

