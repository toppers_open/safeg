#ifndef DUALOSCOM_TARGET_H
#define DUALOSCOM_TARGET_H

#include <stdbool.h>   /* bool type (for filter function) */
#include <stdint.h>    /* uint32_t */
#include "gpos-dualoscom_config.h"

/*
 * TYPES
 */
typedef struct {
    uint32_t num_channels;         // number of channels
    uint32_t num_groups;           // number of groups
    void     *channels2groups;     // bool array[channels][groups]
    uint32_t shmem_pending_events; // offset in shared memory to pending events
} dualoscom_init_data_t;

typedef bool bool_t;

#endif /* DUALOSCOM_TYPES */
