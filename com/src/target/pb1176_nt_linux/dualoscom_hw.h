#ifndef DUALOSCOM_HW
#define DUALOSCOM_HW

#include <linux/ioctl.h>
#include <stdbool.h>
#include <stdint.h>

// 'j' 00-3F   linux/joystick.h
#define DUALOSCOM_IOCTL_CMD_INIT           _IO('j', 0x40)
#define DUALOSCOM_IOCTL_CMD_SENDEVENT      _IOW('j', 0x41, int)
#define DUALOSCOM_IOCTL_CMD_WAITEVENT      _IOW('j', 0x42, int)
#define DUALOSCOM_IOCTL_CMD_WAITGROUP      _IOWR('j', 0x43, int)

#endif /* DUALOSCOM_HW */
