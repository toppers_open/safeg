/*
 * MISC or MANUAL DEVICE
 */
#define ALLOC_DEVICE_MANUALLY

/*
 * ARCHITECTURE
 */
#define __TARGET_ARCH_ARM 6

/*
 * SHARED MEMORY (allocated in kernel parameters)
 */
#define SHM_PHYS_ADDRESS    0x4000000UL /* 64MB */
#define SHM_SIZE            0x100000UL  /* 1MB */

/*
 * GPOS2RTOS INTERRUPTS
 */
#define GPOS2RTOS_FIQ_SOFTINT
// #define GPOS2RTOS_FIQ_WDOG

#ifdef GPOS2RTOS_FIQ_SOFTINT
#include "../../safeg/safeg/nt_safeg_syscalls.h" /* for safeg_raise_softfiq */
#define send_gpos2rtos_irq() safeg_raise_softfiq()
#endif

#ifdef GPOS2RTOS_FIQ_WDOG
#define REG(x) *((volatile uint32_t *)(x))
#define WATCHDOG_BASE wdog                       /* watchdog base register */
#define LOAD_REG      REG(WATCHDOG_BASE + 0x0)   /* value from which we count down, 0 generates irq immediately */
#define CTRL_REG      REG(WATCHDOG_BASE + 0x8)   /* bit1: generate reset bit0: generate interrupt */
#define LOCK_REG      REG(WATCHDOG_BASE + 0xC00) /* write 0x1ACCE551 to enable / other to disable. read: 0=unlocked 1=locked */
static inline void safeg_raise_wdogfiq(void)
{
    static void *wdog;
    wdog = ioremap(0x10107000, 0xFFF); /* remap watchdog interface */
    LOCK_REG     = 0x1ACCE551;
    CTRL_REG     = 1;
    LOAD_REG     = 0;
    LOCK_REG     = 0;
}
#define send_gpos2rtos_irq() safeg_raise_wdogfiq()
#endif

/*
 * RTOS2GPOS INTERRUPTS
 */
#define DUALOSCOM_RTOS2GPOS_IRQ 33