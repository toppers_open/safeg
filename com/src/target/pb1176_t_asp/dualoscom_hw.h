#define GPOS2RTOS_FIQ_SOFTINT
//#define GPOS2RTOS_FIQ_WDOG

#ifdef GPOS2RTOS_FIQ_SOFTINT
#define NT_INT_PRIORITY -2
#define NT_INTNO_SOFTIRQ 1 /* softirq interrupt */
#endif

#ifdef GPOS2RTOS_FIQ_WDOG
#define NT_INT_PRIORITY -2
#define NT_INTNO_SOFTIRQ 0 /* watchdog interrupt */
#endif

#define SHM_PHYS_ADDRESS 0x4000000UL /* 64MB */

static inline void __dualoscom_send_irq(void)
{
#define REALVIEW_DC1176_GIC_DIST_BASE 0x10121000 /* GIC distributor, on devchip */
#define GIC_DIST_PENDING_SET2 (REALVIEW_DC1176_GIC_DIST_BASE + 0x204)
    uint32_t *nt_irq = (uint32_t *)GIC_DIST_PENDING_SET2;
    *nt_irq = 2;
}
