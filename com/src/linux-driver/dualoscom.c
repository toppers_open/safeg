/*
 * SAFEG Dual-OS Communications Driver for Linux
 *
 * Copyright (C) Nagoya University
 * Daniel Sangorrin <daniel.sangorrin@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */
#include <linux/kernel.h>
#include <linux/module.h>      /* for MODULE_AUTHOR..*/
#include <linux/init.h>        /* for module_init */
#include <asm/io.h>            /* for virt_to_phys, ioremap */
#include <linux/semaphore.h>
#include <linux/wait.h>        /* for wait queues */
#include <linux/sched.h>
#include <linux/interrupt.h>   /* for request_irq */
#include <linux/fs.h>          /* for alloc_chrdev_region */
#include <linux/cdev.h>        /* for cdev_alloc */
#include <linux/dma-mapping.h> /* for dma_alloc_coherent */
#include <linux/ioctl.h>
#include <asm/bitops.h>        /* for test_and_set_bit */
#include <linux/slab.h>        /* for kmalloc/kfree */
#include <asm/uaccess.h>       /* for copy_to/from_user */
#include <linux/miscdevice.h>  /* for misc_register */
#include "dualoscom_driver_hw.h" /* for shared memory and interrupts */

/*
 * DEBUG
 */
#ifndef DEBUG
#define DEBUG(enable,x,args...)  \
    if(enable) printk(KERN_INFO "" x, ##args)
#endif

#define DBG_INIT  false
#define DBG_CALLS false

/*
 * MAX VALUES (can be adjusted)
 */
#define MAX_NUM_CHANNELS   1024
#define MAX_NUM_GROUPS     64
#define MAX_PENDING_OFFSET (SHM_SIZE - MAX_NUM_CHANNELS*8)

/*
 * MODULE TYPES
 */
typedef struct {
    uint32_t num_channels;         // number of channels
    uint32_t num_groups;           // number of groups
    bool     *channels2groups;     // bool array[num_channels][num_groups]
    uint32_t shmem_pending_events; // offset in shared memory to pending events
} dualoscom_init_data_t;

typedef struct {
    uint32_t          id;                 // channel id
    uint32_t          *rtos2gpos_pending; // pointer to shared memory
    uint32_t          *gpos2rtos_pending; // pointer to shared memory
    wait_queue_head_t gpos_channel_wq;    // wait queue for channels
} dualoscom_channel_event_t;

typedef struct {
    uint32_t          id;            // group id
    wait_queue_head_t gpos_group_wq; // wait queue for groups
} dualoscom_group_event_t;

typedef struct {
    uint32_t num_channels;                     // number of channels
    uint32_t num_groups;                       // number of groups
    bool     *channels2groups;                 // bool array[channels][groups]
    dualoscom_channel_event_t *channel_events; // array of channel events
    dualoscom_group_event_t   *group_events;   // array of group events
} dualoscom_kernel_t;

/*
 * MODULE VARIABLE DECLARATIONS
 *      initialized: flag to indicate if ioctl init was already called
 *      the_init_mutex: mutex used to protect the initialization process
 *      major: device major number (necessary when unregistering the device)
 *      safeg_cdev: character device (necessary when removing the device)
 *      shmem: shared memory pointer (kernel virtual address)
 *      the_dualoscom_kernel: big structure containing all the information
 */
static bool initialized = false;
static DECLARE_MUTEX(the_init_mutex);
static void *shmem;
static dualoscom_kernel_t the_dualoscom_kernel;
#ifdef ALLOC_DEVICE_MANUALLY
static int major;
static struct cdev safeg_cdev;
#endif

/*
 * OPEN
 */
static int safeg_open(struct inode *inode, struct file *filp)
{
    DEBUG(DBG_CALLS, "OPEN called\n");
    return 0;
}

/*
 * RELEASE
 */
static int safeg_release(struct inode *inode, struct file *filp)
{
    DEBUG(DBG_CALLS, "RELEASE called\n");
    kfree(the_dualoscom_kernel.channel_events);
    if (the_dualoscom_kernel.num_groups > 0) {
        kfree(the_dualoscom_kernel.channels2groups);
        kfree(the_dualoscom_kernel.group_events);
    }
    return 0;
}

/*
 * MMAP
 */
static int safeg_mmap(struct file *filp, struct vm_area_struct *vma)
{
    DEBUG(DBG_CALLS, "MMAP called\n");

    if ((vma->vm_end - vma->vm_start) > SHM_SIZE) return -EAGAIN;

    // note: pgprot_noncached is possible too
    vma->vm_page_prot = pgprot_dmacoherent(vma->vm_page_prot);

    if (remap_pfn_range(vma,
                        vma->vm_start,
                        SHM_PHYS_ADDRESS >> PAGE_SHIFT,
                        vma->vm_end - vma->vm_start,
                        vma->vm_page_prot))
        return -EAGAIN;

    return 0;
}

/*
 * IOCTLs
 */
static int dualoscom_ioctl_init(dualoscom_init_data_t __user *user_init_data)
{
    dualoscom_init_data_t init_data;
    bool *channels2groups;
    int i, j;
    dualoscom_channel_event_t *chevent;
    dualoscom_group_event_t *grevent;

    DEBUG(DBG_INIT, "Lock mutex and check initialized flag\n");

    down(&the_init_mutex);

    if (initialized) {
        DEBUG(DBG_INIT, "Dualoscom was already initialized\n");
        up(&the_init_mutex);
        return 0;
    }

    DEBUG(DBG_INIT, "Get init data from user space\n");

    if (copy_from_user(&init_data, user_init_data, sizeof(init_data)))
        return -EFAULT;

    DEBUG(DBG_INIT, "num_channels:%u, num_groups:%u, shmem_pending:0x%X \
                    channels2groups:0x%X\n",
                    init_data.num_channels, init_data.num_groups,
                    init_data.shmem_pending_events,
                    (uint32_t)init_data.channels2groups);

    if ((init_data.num_channels > MAX_NUM_CHANNELS) ||
        (init_data.num_channels == 0) ||
        (init_data.num_groups   > MAX_NUM_GROUPS)   ||
        (init_data.shmem_pending_events > MAX_PENDING_OFFSET)) {
        DEBUG(DBG_INIT, "Bad init_data parameters\n");
        return -ENOMEM;
    }

    if (init_data.num_groups > 0) {
        DEBUG(DBG_INIT, "kmalloc %d bytes for channels2groups\n",
            init_data.num_channels*init_data.num_groups*sizeof(bool));

        channels2groups = kmalloc(
            init_data.num_channels*init_data.num_groups*sizeof(bool), GFP_KERNEL);
        if (!channels2groups) return -ENOMEM;

        if (copy_from_user(channels2groups, init_data.channels2groups,
                init_data.num_channels*init_data.num_groups*sizeof(bool)))
            return -EFAULT;

        DEBUG(DBG_INIT, "Init the global the_dualoscom_kernel data structure\n");
        the_dualoscom_kernel.num_groups      = init_data.num_groups;
        the_dualoscom_kernel.channels2groups = channels2groups;

        the_dualoscom_kernel.group_events = kmalloc(
            init_data.num_groups*sizeof(dualoscom_group_event_t), GFP_KERNEL);
        if (!the_dualoscom_kernel.group_events) return -ENOMEM;

        DEBUG(DBG_INIT, "Channel to groups:\n");
#define CH2GR(x,y) the_dualoscom_kernel.channels2groups[(y)*(the_dualoscom_kernel.num_groups)+(x)]
        for(i=0; i<init_data.num_channels; i++) {
            DEBUG(DBG_INIT, "Channel %d:", i);
            for(j=0; j<init_data.num_groups; j++) {
                DEBUG(DBG_INIT, " %s", CH2GR(i,j)?"true":"false");
            }
            DEBUG(DBG_INIT, "\n");
        }

        DEBUG(DBG_INIT, "Initialize group events\n");
        for(i=0; i<init_data.num_groups; i++) {
            grevent = &the_dualoscom_kernel.group_events[i];
            grevent->id = i;
            init_waitqueue_head(&grevent->gpos_group_wq);
        }
    } else {
        DEBUG(DBG_INIT, "No groups, so set to 0 and NULL\n");
        the_dualoscom_kernel.num_groups       = 0;
        the_dualoscom_kernel.channels2groups  = NULL;
        the_dualoscom_kernel.group_events     = NULL;
    }

    the_dualoscom_kernel.num_channels    = init_data.num_channels;

    the_dualoscom_kernel.channel_events  = kmalloc(
        init_data.num_channels*sizeof(dualoscom_channel_event_t), GFP_KERNEL);
    if (!the_dualoscom_kernel.channel_events) return -ENOMEM;

    DEBUG(DBG_INIT, "Initialize channel events\n");
    for(i=0; i<init_data.num_channels; i++) {
        chevent = &the_dualoscom_kernel.channel_events[i];
        chevent->id                = i;
        chevent->rtos2gpos_pending = (uint32_t *)(shmem + init_data.shmem_pending_events) + i;
        DEBUG(DBG_INIT, "chevent->rtos2gpos_pending: 0x%X (val:0x%X)\n",
              (uint32_t)chevent->rtos2gpos_pending, *(chevent->rtos2gpos_pending));
        chevent->gpos2rtos_pending = chevent->rtos2gpos_pending + init_data.num_channels;
        DEBUG(DBG_INIT, "chevent->gpos2rtos_pending: 0x%X (val:0x%X)\n",
              (uint32_t)chevent->gpos2rtos_pending, *(chevent->gpos2rtos_pending));
        init_waitqueue_head(&chevent->gpos_channel_wq);
    }

    DEBUG(DBG_INIT, "Unlock the mutex after setting the initialized flag\n");
    initialized = true;
    up(&the_init_mutex);

    DEBUG(DBG_INIT, "Send a signal to the rtos\n");
    send_gpos2rtos_irq();

    return 0;
}

static int dualoscom_ioctl_sendevent(uint32_t channel_id)
{
    dualoscom_channel_event_t *chevent;

    if (!initialized) return -1;
    if (channel_id >= the_dualoscom_kernel.num_channels) return -1;

    chevent = &the_dualoscom_kernel.channel_events[channel_id];

    *(chevent->gpos2rtos_pending) = 1;

    send_gpos2rtos_irq();

    return 0;
}

static int dualoscom_ioctl_waitevent(uint32_t channel_id)
{
    dualoscom_channel_event_t *chevent;

    if (!initialized) return -1;
    if (channel_id >= the_dualoscom_kernel.num_channels) return -1;

    chevent = &the_dualoscom_kernel.channel_events[channel_id];

    wait_event_interruptible(chevent->gpos_channel_wq,
                             *(chevent->rtos2gpos_pending) != 0);

    *(chevent->rtos2gpos_pending) = 0;

    return 0;
}

// command numbers 'j' 00-3F   linux/joystick.h
#define DUALOSCOM_IOCTL_CMD_INIT           _IO('j', 0x40)
#define DUALOSCOM_IOCTL_CMD_SENDEVENT      _IOW('j', 0x41, int)
#define DUALOSCOM_IOCTL_CMD_WAITEVENT      _IOW('j', 0x42, int)
#define DUALOSCOM_IOCTL_CMD_WAITGROUP      _IOWR('j', 0x43, int)

static int safeg_ioctl(struct inode *inode, struct file *filp,
                       unsigned int cmd, unsigned long arg)
{
    DEBUG(DBG_CALLS, "IOCTL called (cmd:%u, arg:%lu)\n", cmd, arg);

    switch(cmd) {
        case DUALOSCOM_IOCTL_CMD_INIT:
            DEBUG(DBG_INIT, "DUALOSCOM_IOCTL_CMD_INIT(%u)\n", cmd);
            return dualoscom_ioctl_init((dualoscom_init_data_t *)arg);
            break;
        case DUALOSCOM_IOCTL_CMD_SENDEVENT:
            DEBUG(DBG_CALLS, "DUALOSCOM_IOCTL_CMD_SENDEVENT(%u)\n", cmd);
            return dualoscom_ioctl_sendevent((uint32_t)arg);
            break;
        case DUALOSCOM_IOCTL_CMD_WAITEVENT:
            DEBUG(DBG_CALLS,
                  "DUALOSCOM_IOCTL_CMD_WAITEVENT(cmd:%u arg:%lu)\n", cmd, arg);
            return dualoscom_ioctl_waitevent((uint32_t)arg);
            break;
        default:
            DEBUG(DBG_CALLS, "IOCTL cmd (%u): not available\n", cmd);
            return -ENOTTY;
    }
}

static struct file_operations safeg_fops = {
    .owner   = THIS_MODULE,
    .open    = safeg_open,
    .mmap    = safeg_mmap,
    .ioctl   = safeg_ioctl,
    .release = safeg_release,
};

#ifndef ALLOC_DEVICE_MANUALLY
static struct miscdevice safeg_dev = {
   .minor = MISC_DYNAMIC_MINOR,
   .name = "safeg",
   .fops = &safeg_fops,
};
#else
static dev_t dev;
#endif

/*
 * IRQ HANDLER
 */
static irqreturn_t safeg_dualoscom_irq_handler(int irq, void *dev_id)
{
    int i;
    dualoscom_channel_event_t *chevent;

    DEBUG(DBG_CALLS, "SafeG IRQ handler\n");

    if (!initialized) {
        DEBUG(DBG_CALLS, "BUG: received irq while not initialized\n");
        return IRQ_HANDLED;
    }

    // TODO: groups

    for(i=0; i<the_dualoscom_kernel.num_channels; i++) {
        chevent = &the_dualoscom_kernel.channel_events[i];
        if (*(chevent->rtos2gpos_pending) == 0) continue;
        wake_up_interruptible(&chevent->gpos_channel_wq);
    }

    return IRQ_HANDLED;
}

/*
 * MODULE INIT
 */
static int __init safeg_init(void)
{
    int err;

    DEBUG(DBG_INIT, "SafeG allocate shared memory\n");
#if __TARGET_ARCH_ARM == 6
    shmem = ioremap_nocache(SHM_PHYS_ADDRESS, SHM_SIZE);
#else
    shmem = ioremap_cached(SHM_PHYS_ADDRESS, SHM_SIZE);
#endif

    DEBUG(DBG_INIT, "Shared Memory: 0x%X mapped to 0x%X\n",
                    (u32)SHM_PHYS_ADDRESS, (u32)shmem);

    DEBUG(DBG_INIT, "Register SafeG char device\n");
#ifndef ALLOC_DEVICE_MANUALLY
    err = misc_register(&safeg_dev);
    if (err) goto init_error;
#else
    DEBUG(DBG_INIT, "Allocating SafeG major/minor\n");
    err = alloc_chrdev_region(&dev, 0, 1,"safeg");
    major = MAJOR(dev);

    DEBUG(DBG_INIT, "mknod /dev/safeg c %d 0 (/proc/devices)\n", major);
    DEBUG(DBG_INIT, "chmod 0664 /dev/safeg\n");

    DEBUG(DBG_INIT, "Register SafeG char device\n");
    cdev_init(&safeg_cdev, &safeg_fops);
    safeg_cdev.owner = THIS_MODULE;
    err = cdev_add (&safeg_cdev, dev, 1);
    if (err) goto init_error;
#endif

    DEBUG(DBG_INIT, "Request interrupt\n");
    err = request_irq(DUALOSCOM_RTOS2GPOS_IRQ, safeg_dualoscom_irq_handler, 0, "safeg-comm", NULL);
    if (err) goto init_error;

    return 0;

init_error:
    DEBUG(DBG_INIT, "Error %d at initialization", err);
    return err;
}

/*
 * MODULE EXIT
 */
static void __exit safeg_exit(void)
{
    DEBUG(DBG_INIT, "Goodbye SAFEG\n");
    free_irq(DUALOSCOM_RTOS2GPOS_IRQ, NULL);
#ifndef ALLOC_DEVICE_MANUALLY
    misc_deregister(&safeg_dev);
#else
    cdev_del(&safeg_cdev);
    unregister_chrdev_region(MKDEV(major, 0), 1);
#endif
}

module_init(safeg_init);
module_exit(safeg_exit);

MODULE_AUTHOR("Daniel Sangorrin <daniel.sangorrin@gmail.com>");
MODULE_DESCRIPTION("SAFEG dualoscom driver");
MODULE_LICENSE("GPL");
