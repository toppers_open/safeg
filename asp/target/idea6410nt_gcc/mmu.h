#ifndef MMU_H
#define MMU_H

#include "kernel_impl.h"

extern void idea6410_mmu_init(void);
extern void idea6410_cache_init();

#endif /* MMU_H */
