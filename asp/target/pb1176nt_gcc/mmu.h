#ifndef MMU_H
#define MMU_H

#include "kernel_impl.h"

extern void pb1176_mmu_init(void);
extern void pb1176_cache_init();

#endif /* MMU_H */
