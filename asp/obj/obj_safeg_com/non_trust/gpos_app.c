#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "dualoscom.h"
#include "gpos-dualoscom_config.h"

bool filter_even(void *buffer, uint32_t size)
{
    uint32_t value;

    value = *((uint32_t *)buffer);

    if ((value % 2) == 0)  return true;
    return false;
}

// #define SMEM_SIZE 64
//
// void
// share_mem_test() {
// 	int ret;
// 	void *buffer;
// 	uint32_t size;
// #if 0
// 	dualoscom_block_id_t block_id;
// #endif
// 	int i;
//
// 	printf("Share mem test start!.\n");
// 	fflush(stdout);
// #if 0
// 	do {
// 		ret = dualoscom_block_alloc(DUALOSCOM_CHANNEL_2, &block_id);
// 		if(ret != 0) {
// 			if (ret != DUALOSCOM_FULL){
// 				printf("Alloc Error!! %d\n", ret);
// 				fflush(stdout);
// 			}
// 		}
// 	}while(ret != 0);
// #endif
// 	ret = dualoscom_block_getbuffer(DUALOSCOM_SMEM_1, 0, &buffer, &size);
// 	if(ret != 0) {
// 		printf("Get Buffer Error!! %d\n",ret);
// 		fflush(stdout);
// 	}
// #if 0
// 	ret = dualoscom_block_free(DUALOSCOM_CHANNEL_2, block_id);
// 	if(ret != 0) {
// 		printf("Block Free Error!! %d\n",ret);
// 		fflush(stdout);
// 	}
// #endif
// 	printf("buffer 0x%x, Size %d.\n", (unsigned int)buffer, size);
// 	fflush(stdout);
//
// 	while(1) {
// 		for(i = 0; i < SMEM_SIZE/8; i++){
// 			printf("%d : %d, ", i, *((uint32_t *)buffer + i));
// 		}
// 		printf("\n");
// 		sleep(2);
// 	}
// }

int main()
{
    int ret;
    void *buffer;
    uint32_t size;
    dualoscom_block_id_t block_id;
    char msg[] = "hey!";

    printf("GPOS dualoscom_init\n");
    ret = dualoscom_init(true, 0);
    assert(ret == 0);

    printf("Dual-OS communications initialized\n");

    printf("GPOS wait for message\n");
    ret = dualoscom_channel_event_wait(DUALOSCOM_CHANNEL_0, 0);
    assert(ret == 0);

    printf("Event received\n");

    ret = dualoscom_block_dequeue(DUALOSCOM_CHANNEL_0, &block_id);
    assert(ret == 0);

    ret = dualoscom_block_getbuffer(DUALOSCOM_CHANNEL_0, block_id, &buffer, &size);
    assert(ret == 0);

    printf("MSG: %s\n", (char *)buffer);

    printf("GPOS send ack\n");
    ret = dualoscom_channel_event_send(DUALOSCOM_CHANNEL_0);
    assert(ret == 0);

    printf("GPOS wait for rpc\n");
    ret = dualoscom_channel_event_wait(DUALOSCOM_CHANNEL_0, 0);
    assert(ret == 0);

    printf("received params: %u and %u\n",
           *((uint32_t *)buffer), *((uint32_t *)(buffer + 4)));

    *((uint32_t *)buffer) = *((uint32_t *)buffer) * *((uint32_t *)(buffer + 4));
    printf("GPOS send rpc result (%u)\n", *((uint32_t *)buffer));

    ret = dualoscom_channel_event_send(DUALOSCOM_CHANNEL_0);
    assert(ret == 0);

    printf("GPOS wait for rpc\n");
    ret = dualoscom_channel_event_wait(DUALOSCOM_CHANNEL_0, 0);
    assert(ret == 0);

    printf("received params: %u and %u\n",
           *((uint32_t *)buffer), *((uint32_t *)(buffer + 4)));

    *((uint32_t *)buffer) = *((uint32_t *)buffer) * *((uint32_t *)(buffer + 4));
    printf("GPOS send rpc result (%u)\n", *((uint32_t *)buffer));

    ret = dualoscom_channel_event_send(DUALOSCOM_CHANNEL_0);
    assert(ret == 0);

    printf("GPOS dualoscom_block_free\n");
    ret = dualoscom_block_free(DUALOSCOM_CHANNEL_0, block_id);
    assert(ret == 0);

    printf("Send a message to the RTOS by channel 1\n");
    ret = dualoscom_block_alloc(DUALOSCOM_CHANNEL_1, &block_id);
    assert(ret == 0);

    ret = dualoscom_block_getbuffer(DUALOSCOM_CHANNEL_1, block_id, &buffer, &size);
    assert(ret == 0);

    assert(sizeof(msg) < size);
    memcpy(buffer, (void *)msg, sizeof(msg));

    ret = dualoscom_block_enqueue(DUALOSCOM_CHANNEL_1, block_id);
    assert(ret == 0);

    ret = dualoscom_channel_event_send(DUALOSCOM_CHANNEL_1);
    assert(ret == 0);

    printf("Send several messages with a filter on\n");

    ret = dualoscom_block_alloc(DUALOSCOM_CHANNEL_0, &block_id);
    assert(ret == 0);

    ret = dualoscom_block_getbuffer(DUALOSCOM_CHANNEL_0, block_id, &buffer, &size);
    assert(ret == 0);

    *((uint32_t *)buffer) = 1;

    ret = dualoscom_block_enqueue(DUALOSCOM_CHANNEL_0, block_id);
    if(ret != 0) printf("data %u filtered\n",*((uint32_t *)buffer));

    ret = dualoscom_block_alloc(DUALOSCOM_CHANNEL_0, &block_id);
    assert(ret == 0);

    ret = dualoscom_block_getbuffer(DUALOSCOM_CHANNEL_0, block_id, &buffer, &size);
    assert(ret == 0);

    *((uint32_t *)buffer) = 2;

    ret = dualoscom_block_enqueue(DUALOSCOM_CHANNEL_0, block_id);
    if(ret != 0) printf("data %u filtered\n",*((uint32_t *)buffer));

    ret = dualoscom_block_alloc(DUALOSCOM_CHANNEL_0, &block_id);
    assert(ret == 0);

    ret = dualoscom_block_getbuffer(DUALOSCOM_CHANNEL_0, block_id, &buffer, &size);
    assert(ret == 0);

    *((uint32_t *)buffer) = 3;

    ret = dualoscom_block_enqueue(DUALOSCOM_CHANNEL_0, block_id);
    if(ret != 0) printf("data %u filtered\n",*((uint32_t *)buffer));

    ret = dualoscom_channel_event_send(DUALOSCOM_CHANNEL_0);
    assert(ret == 0);

// 	share_mem_test();

    return 0;
}
