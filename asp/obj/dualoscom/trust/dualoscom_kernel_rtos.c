#include "dualoscom_types.h"
#include "dualoscom_config.h"
#include "kernel_cfg.h"
#include "pb1176t.h"

#if DUALOSCOM_NUM_CHANNELS == 0
#error "No channels defined. You should at least define 1 channel."
#endif

/*
 * INTERNAL TYPES
 */
typedef struct {
    ID       rtos_sem;
    uint32_t *rtos2gpos_pending;
    uint32_t *gpos2rtos_pending;
#ifdef DUALOSCOM_NUM_GROUPS
    bool_t   groups[DUALOSCOM_NUM_GROUPS];
#endif
} dualoscom_channel_event_t;

typedef struct {
    ID        rtos_sem;
    uint32_t  waiting;
} dualoscom_group_event_t;

typedef struct {
    bool_t initialized;
    dualoscom_channel_event_t channel_events[DUALOSCOM_NUM_CHANNELS];
#ifdef DUALOSCOM_NUM_GROUPS
    dualoscom_group_event_t group_events[DUALOSCOM_NUM_GROUPS];
#endif
} dualoscom_kernel_t;

/*
 * DEBUG
 */
// #define DUALOSCOM_DEBUG

#ifdef DUALOSCOM_DEBUG

static void __dualoscom_print_chevent(dualoscom_channel_event_t *ev)
{
    ER      ercd;
    T_RSEM  rsem;

    syslog(LOG_NOTICE, "chevent->rtos2gpos_pending: 0x%X (val:0x%X)\n", (uint32_t)ev->rtos2gpos_pending, *(ev->rtos2gpos_pending));
    syslog(LOG_NOTICE, "chevent->gpos2rtos_pending: 0x%X (val:0x%X)\n", (uint32_t)ev->gpos2rtos_pending, *(ev->gpos2rtos_pending));

    ercd = ref_sem(ev->rtos_sem, &rsem);
    assert(ercd == E_OK);

    if (rsem.wtskid == TSK_NONE) {
        syslog(LOG_NOTICE, "rtos_sem no task waiting");
    } else {
        syslog(LOG_NOTICE, "rtos_sem task with id %d waiting", rsem.wtskid);
    }
    syslog(LOG_NOTICE, "rtos_sem value: %d", rsem.semcnt);
}

#endif

/*
 * MODULE VARIABLES
 */
static dualoscom_kernel_t the_dualoscom_kernel;

/*
 * EVENTS HANDLER
 */
void dualoscom_handler(void)
{
    ER ercd;
    int i;
    dualoscom_channel_event_t *chevent;
#ifdef DUALOSCOM_NUM_GROUPS
    int j;
    dualoscom_group_event_t *grevent;
#endif
    bool_t group_signaled;

#ifdef DUALOSCOM_DEBUG
    syslog(LOG_EMERG, "Dualoscom handler");
#endif

#ifdef GPOS2RTOS_FIQ_WDOG
#define REG(x) *((volatile uint32_t *)(x))
#define WATCHDOG_BASE 0x10107000                 /* watchdog base register */
#define CTRL_REG      REG(WATCHDOG_BASE + 0x8)   /* bit1: generate reset bit0: generate interrupt */
#define INTCLEAR_REG  REG(WATCHDOG_BASE + 0xC)   /* clear interrupt and reload counter */
#define INTRAW_REG    REG(WATCHDOG_BASE + 0x10)  /* bit0: if 1, there is an interrupt pending */
#define LOCK_REG      REG(WATCHDOG_BASE + 0xC00) /* write 0x1ACCE551 to enable / other to disable. read: 0=unlocked 1=locked */
    LOCK_REG     = 0x1ACCE551;
    CTRL_REG     = 0;
    INTCLEAR_REG = 1;
    assert(INTRAW_REG == 0);
    LOCK_REG     = 0;
#endif

    if (the_dualoscom_kernel.initialized == false) {
        ercd = isig_sem(DUALOSCOM_SEM_INIT);
        assert(ercd == E_OK);
        the_dualoscom_kernel.initialized = true;
        return;
    }

    // else signal all channels with pending events
    for(i=0; i<DUALOSCOM_NUM_CHANNELS; i++) {
        chevent = &the_dualoscom_kernel.channel_events[i];
        if (*(chevent->gpos2rtos_pending) == 0) continue;

        // signal group (if a task is waiting)
        group_signaled = false;
#ifdef DUALOSCOM_NUM_GROUPS
        for(j=0; j<DUALOSCOM_NUM_GROUPS; j++) {
            if (chevent->groups[j] == false) continue;
            grevent = &the_dualoscom_kernel.group_events[j];
            if (grevent->waiting == 0) continue;
            isig_sem(grevent->rtos_sem);
            *(chevent->gpos2rtos_pending) = 0;
            group_signaled = true;
        }
#endif
        // otherwise signal the channel
        if (group_signaled == true) continue;
        isig_sem(chevent->rtos_sem);
        *(chevent->gpos2rtos_pending) = 0;
    }
}

void __dualoscom_send_irq(void)
{
    #define REALVIEW_DC1176_GIC_DIST_BASE 0x10121000 /* GIC distributor, on devchip */
    #define GIC_DIST_PENDING_SET2 (REALVIEW_DC1176_GIC_DIST_BASE + 0x204)
    static uint32_t *nt_irq = (uint32_t *)GIC_DIST_PENDING_SET2;
    *nt_irq = 2;
}

/*
 * INIT
 */
int __dualoscom_init_kernel(dualoscom_time_t timeout, void *shmem)
{
    ER ret;
    int i;
    dualoscom_channel_event_t *chevent;
#ifdef DUALOSCOM_NUM_GROUPS
    int j;
    dualoscom_group_event_t *grevent;
#endif
#ifdef DUALOSCOM_NUM_GROUPS
    bool_t groups[DUALOSCOM_NUM_CHANNELS][DUALOSCOM_NUM_GROUPS] = DUALOSCOM_CHANNEL2GROUPS;
    ID group_semaphores[DUALOSCOM_NUM_GROUPS] = DUALOSCOM_GROUP_SEMAPHORES;
#endif
    ID semaphores[DUALOSCOM_NUM_CHANNELS] = DUALOSCOM_SEMAPHORES;

    // 1.- initialize channel events
    for(i=0; i<DUALOSCOM_NUM_CHANNELS; i++) {
        chevent = &the_dualoscom_kernel.channel_events[i];
        chevent->rtos_sem = semaphores[i];
        chevent->rtos2gpos_pending = (uint32_t *)(shmem + DUALOSCOM_SHMEM_PENDING_EVENTS) + i;
        chevent->gpos2rtos_pending = chevent->rtos2gpos_pending + DUALOSCOM_NUM_CHANNELS;
#ifdef DUALOSCOM_DEBUG
        __dualoscom_print_chevent(chevent);
#endif
#ifdef DUALOSCOM_NUM_GROUPS
        for(j=0; j<DUALOSCOM_NUM_GROUPS; j++) {
            chevent->groups[j] = groups[i][j];
        }
#endif
    }

#ifdef DUALOSCOM_NUM_GROUPS
    // 2.- initialize group events
    for(i=0; i<DUALOSCOM_NUM_GROUPS; i++) {
        grevent = &the_dualoscom_kernel.group_events[i];
        grevent->rtos_sem = group_semaphores[i];
        grevent->waiting  = 0;
    }
#endif

    // 3.- wait for the gpos
    the_dualoscom_kernel.initialized = false;

    ret = ena_int(NT_INTNO_SOFTIRQ);
    assert(ret == E_OK);

    ret = wai_sem(DUALOSCOM_SEM_INIT);
    assert(ret == E_OK);

    return 0;
}

int __dualoscom_kernel_sendevent(dualoscom_channel_id_t channel_id)
{
    dualoscom_channel_event_t  *ev;

    ev = &the_dualoscom_kernel.channel_events[channel_id];
    *(ev->rtos2gpos_pending) = 1;

    __dualoscom_send_irq();

    return 0;
}

int __dualoscom_kernel_waitevent(dualoscom_channel_id_t channel_id,
                                 dualoscom_time_t timeout)
{
    dualoscom_channel_event_t  *ev;

    ev = &the_dualoscom_kernel.channel_events[channel_id];

#ifdef DUALOSCOM_DEBUG
    __dualoscom_print_chevent(ev);
#endif

    wai_sem(ev->rtos_sem);

    return 0;
}

int __dualoscom_kernel_waitgroup(dualoscom_group_id_t group_id,
                                 dualoscom_time_t timeout)
{
#ifdef DUALOSCOM_NUM_GROUPS
    dualoscom_group_event_t  *gr;

    gr = &the_dualoscom_kernel.group_events[group_id];

    gr->waiting++;
    wai_sem(gr->rtos_sem);
    gr->waiting--;
#endif
    return 0;
}
