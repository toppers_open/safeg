// #define GPOS2RTOS_FIQ_SOFTINT
#define GPOS2RTOS_FIQ_WDOG

#ifdef GPOS2RTOS_FIQ_SOFTINT
#define NT_INT_PRIORITY -2
#define NT_INTNO_SOFTIRQ 1 /* softirq interrupt */
#endif

#ifdef GPOS2RTOS_FIQ_WDOG
#define NT_INT_PRIORITY -2
#define NT_INTNO_SOFTIRQ 0 /* watchdog interrupt */
#endif
