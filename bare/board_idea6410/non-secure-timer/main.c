/*
 *  TOPPERS/SafeG Dual-OS monitor
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Safety Gate Dual-OS monitor
 *
 *  Copyright (C) 2009-2011 by Embedded and Real-Time Systems Laboratory
 *     Graduate School of Information Science, Nagoya University, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 */
#define Asm __asm__ volatile
#define CP15_CONTROL_WRITE(x) Asm("mcr p15, 0, %0, c1, c0, 0"::"r"(x))
#define CP15_PPMR_WRITE(x) Asm("mcr p15, 0, %0, c15, c2, 4"::"r"(x))
#define CP15_DCACHE_CLEAN_AND_INVALIDATE() Asm("mcr p15, 0, %0, c7, c14, 0":: "r"(0))
#define CP15_PBUFFER_FLUSH()     Asm("mcr p15, 0, %0, c7, c5, 4"::"r"(0))

#define CP15_SET_VBAR(x)    \
    __asm__ volatile ("mcr p15, 0, %0, c12, c0, 0"::"r"(x))

extern void *vector_table;
extern void timer_init(void (*callback)(void));
extern void uart_init(void);
extern void uart_putc(char c);

void the_timer_callback(void)
{
		uart_putc('n');
		uart_putc('t');
		uart_putc(' ');
}

void exc_c_handler(void)
{
	uart_putc('E'); uart_putc('X'); uart_putc('C');
}

int main(void)
{
	CP15_SET_VBAR(&vector_table);
	CP15_PPMR_WRITE(0x70000013);        // set peripheral memory
	CP15_PBUFFER_FLUSH();               // flush prefetch buffer
	CP15_DCACHE_CLEAN_AND_INVALIDATE(); // invalidate cache
	CP15_CONTROL_WRITE(0);              // disable cache and mmu

	uart_init();
	timer_init(the_timer_callback);

	while(1);

	return 0;
}
