/*
 *  TOPPERS/SafeG Dual-OS monitor
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Safety Gate Dual-OS monitor
 *
 *  Copyright (C) 2009-2011 by Embedded and Real-Time Systems Laboratory
 *     Graduate School of Information Science, Nagoya University, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 */
#define Asm __asm__ volatile
#define CP15_CONTROL_WRITE(x) Asm("mcr p15, 0, %0, c1, c0, 0"::"r"(x))
#define CP15_SET_VBAR(x) Asm("mcr p15, 0, %0, c12, c0, 0"::"r"(x))
#define CP15_PPMR_WRITE(x) Asm("mcr p15, 0, %0, c15, c2, 4"::"r"(x))
#define CP15_DCACHE_CLEAN_AND_INVALIDATE() Asm("mcr p15, 0, %0, c7, c14, 0":: "r"(0))
#define CP15_PBUFFER_FLUSH()     Asm("mcr p15, 0, %0, c7, c5, 4"::"r"(0))
#define CP15_DATA_SYNC_BARRIER() Asm("mcr p15, 0, %0, c7, c10, 4"::"r"(0))

#define PCLK_GATE               (0x7e00f034)
#define S3C64XX_PA_GPIO         (0x7F008000)
#define S3C64XX_GPM_BASE        (S3C64XX_PA_GPIO + 0x0800)
#define S3C64XX_GPMCON0         (S3C64XX_GPM_BASE + 0x00)
#define S3C64XX_GPMCON1         (S3C64XX_GPM_BASE + 0x04)
#define S3C64XX_GPMDAT          (S3C64XX_GPM_BASE + 0x08)
#define S3C64XX_GPMPUD          (S3C64XX_GPM_BASE + 0x0c)

extern void *vector_table;
extern void uart_init(void);
extern void uart_putc(char c);
extern void timer_init(void (*callback)(void));
extern void tzpc_init(void);

void the_timer_callback(void)
{
	static unsigned int p = 0;

	if (p++ % 2)
		*((volatile unsigned int *)S3C64XX_GPMDAT) = 0xFFFFFFFF;
	else
		*((volatile unsigned int *)S3C64XX_GPMDAT) = 0x00000000;

	CP15_DATA_SYNC_BARRIER();
}

void exc_c_handler(void)
{
	uart_putc('E'); uart_putc('X'); uart_putc('C');
}

int main(void)
{
	CP15_SET_VBAR(&vector_table);
	CP15_PPMR_WRITE(0x70000013);        // set peripheral memory
	CP15_PBUFFER_FLUSH();               // flush prefetch buffer
	CP15_DCACHE_CLEAN_AND_INVALIDATE(); // invalidate cache
	CP15_CONTROL_WRITE(0);              // disable cache and mmu

	uart_init();
	*((volatile unsigned int *)PCLK_GATE)      = 0xFFFFFFFF;
	*((volatile unsigned int *)S3C64XX_GPMCON0) = 0x11111111;
	timer_init(the_timer_callback);
	tzpc_init();

	while(1);

	return 0;
}
