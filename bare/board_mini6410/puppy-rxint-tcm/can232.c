/*
 *  TOPPERS/SafeG Dual-OS monitor
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Safety Gate Dual-OS monitor
 *
 *  Copyright (C) 2009-2011 by Embedded and Real-Time Systems Laboratory
 *     Graduate School of Information Science, Nagoya University, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 */
#define Asm __asm__ volatile
#define CP15_DATA_SYNC_BARRIER() Asm("mcr p15, 0, %0, c7, c10, 4"::"r"(0))

typedef unsigned char	uint8_t;	/* 符号無し8ビット整数 */
typedef unsigned short	uint16_t;	/* 符号無し16ビット整数 */
typedef unsigned int	uint32_t;	/* 符号無し32ビット整数 */
typedef int				bool_t;		/* 真偽値 */
typedef unsigned char	char_t;		/* 符号無しの文字型 */
typedef unsigned int	uint_t;		/* 自然なサイズの符号無し整数 */
#define true		1			/* 真 */
#define false		0			/* 偽 */

typedef struct {
	volatile unsigned long	ULCON;
	volatile unsigned long	UCON;
	volatile unsigned long	UFCON;
	volatile unsigned long	UMCON;
	volatile unsigned long	UTRSTAT;
	volatile unsigned long	UERSTAT;
	volatile unsigned long	UFSTAT;
	volatile unsigned long	UMSTAT;
	volatile unsigned char	UTXH;
	volatile unsigned char	res1[3];
	volatile unsigned char	URXH;
	volatile unsigned char	res2[3];
	volatile unsigned long	UBRDIV;
	volatile unsigned long	UDIVSLOT;
	volatile unsigned long	INTP;
	volatile unsigned long	UINTSP;
	volatile unsigned long	UINTM;
} S3C64XX_UART;

typedef unsigned long long int uint64_t;

static volatile int waiting_fiq;

extern void serial_wri_dat(const char *buf, uint32_t len);
extern void serial_rea_dat(char *buf, uint32_t len);
extern void tzic_init(uint64_t int_bit_pattern);

static void can232_uart_init(void)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;

	waiting_fiq = 1;

	uart->UFCON = 0;   // FIFO disabled
	uart->UMCON = 0;   // RTS AFC disabled
	uart->ULCON = 0x3; // 8-N-1
	uart->UCON = 0xE05; // 0xe45;  // polling mode - external clock
	uart->UBRDIV = 106; // integer part of DIV_VAL => (66MHz / (38400 x 16)) ‐ 1 = 106.42
	uart->UDIVSLOT = 0x54AA; // 0.42 * 16 = number of 1s in UDIVSLOT = 7
	uart->UINTM = 0xF; // Mask interrupts
	uart->UINTSP = 0xF; // Clear interrupts
	uart->INTP = 0xF; // Clear interrupts

	tzic_init(0x10000000000); // UART_FIQ3 40

	__asm__ volatile ("msr cpsr, #(0x13|0x80)");
}

static void can232_serial_wri_chr(char_t c)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;

	while (!(uart->UTRSTAT & 0x2));
	uart->UTXH = c;
}

static volatile char_t mychar;

void fiq_c_handler(void)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;

	uart->UINTM = 0xF; // mask receive irq

// 	while (!(uart->UTRSTAT & 0x1) && !(uart->UERSTAT & 0x3));
	mychar = uart->URXH & 0xff;

	uart->INTP = 0x1;  // clear rx interrupt
	waiting_fiq = 0;
}

static char_t can232_serial_rea_chr(void)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;

	uart->UINTM = 0xE; // unmask receive irq

	while(waiting_fiq);
	waiting_fiq = 1;

	return mychar;
}

static void can232_serial_wri_dat(const char_t *buf, uint32_t len)
{
	while (len > 0) {
		can232_serial_wri_chr(*buf++);
		len--;
	}
}

static void can232_serial_rea_dat(char_t *buf, uint32_t len)
{
	while (len > 0) {
		*buf = can232_serial_rea_chr();
		buf++;
		len--;
	}
}

/*
 *  CAN232のオープン
 */
bool_t can232_open(void)
{
	char_t c[10];

	can232_uart_init();

	mychar = 0;

	serial_wri_dat("cross\n", 6);

	/*
	 *  クローズ
	 */
	can232_serial_wri_dat((char_t *)"C\r", 2);
	can232_serial_rea_dat(c, 1);

	serial_wri_dat("speed\n", 6);

	/*
	 *  スピードの設定
	 */
	can232_serial_wri_dat((char_t *)"S6\r", 3);
	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r') {
		serial_wri_dat("ERROR speed\n", 12);
		return false;
	}

	serial_wri_dat("open\n", 5);

	/*
	 *  オープン
	 */
	can232_serial_wri_dat((char_t *)"O\r", 2);
	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r') {
		serial_wri_dat("ERROR open", 10);
		return false;
	}

	return true;
}

static uint_t str2hex(uint8_t *pstr, uint_t length)
{
	int i;
	uint_t val = 0;
	
	for(i = 0; i < length; i++) {
		if ( pstr[i] >= '0' && pstr[i] <= '9') {
			val +=  (pstr[i] - '0') << ((length - i -1) * 4);
		}else{
			val +=  (0xa + (pstr[i] - 'A')) << ((length - i -1) * 4);
		}
	}

	return val;
}

static int Phase;
static int Phase1;

bool_t can232_receive(uint16_t *pcan_id, uint8_t *pcan_length, uint8_t *pcan_data)
{
	char_t  c[20];
	int     i;

    Phase = 0;
    Phase1 = 0;
	do{
		can232_serial_rea_dat(c, 1);
		/* 't'が来るのを待つ */
        Phase1++;
	} while (c[0] != 't');
    Phase = 1;
	/* ID を取得 */
	can232_serial_rea_dat(c, 3);
	*pcan_id = str2hex(c, 3);
    Phase = 2;
	/* Length を取得 */
	can232_serial_rea_dat(c, 1);
	*pcan_length = str2hex(c, 1);

    Phase = 3;
	can232_serial_rea_dat(c, *pcan_length*2);

	for(i = 0; i < *pcan_length; i++){
		pcan_data[i] = str2hex(&c[i * 2], 2);
	}
    Phase = 4;
	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r')  {
		serial_wri_dat("ERROR receive\n", 14);
		return false;
	}
    Phase = 5;

	return true;
}

static void hex2str(uint8_t *pstr, uint_t hex, uint_t length)
{
	int i;

	for(i = length - 1; i >= 0; i--) {
		if ((hex & 0x0f) >= 0 && (hex & 0x0f) <= 9) {
			pstr[i] = '0' + (hex & 0x0f);
		}
		else {
			pstr[i] = 'A' + ((hex & 0x0f) - 0xA);
		}
		hex = hex >> 4;
	}
}

bool_t can232_send(uint16_t can_id, uint8_t can_length, uint8_t *pcan_data)
{
	uint8_t str_can_id[3];
	uint8_t str_can_length[1];
	uint8_t str_can_data[16];
	char_t  c[2];
	int     i;

	hex2str(str_can_id, can_id, 3);
	hex2str(str_can_length, can_length, 1);
	for(i = 0; i < can_length; i++){
		hex2str(&str_can_data[i*2], pcan_data[i], 2);
	}

    Phase = 6;
	can232_serial_wri_dat((char_t *)"t", 1);
    Phase = 7;
	can232_serial_wri_dat((char_t *)str_can_id, 3);
    Phase = 8;
	can232_serial_wri_dat((char_t *)str_can_length, 1);
    Phase = 9;
	can232_serial_wri_dat((char_t *)str_can_data, can_length * 2);
    Phase = 10;
	can232_serial_wri_dat((char_t *)"\r", 1);
    Phase = 11;
	can232_serial_rea_dat(c, 1);
	if (c[0] != 'z')  {
		return false;
	}

	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r')  {
		serial_wri_dat("ERROR send\n", 11);
		return false;
	}

	return true;
}
