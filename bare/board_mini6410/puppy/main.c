typedef unsigned char		uint8_t;	/* 符号無し8ビット整数 */
typedef signed short		int16_t;	/* 符号付き16ビット整数 */
typedef unsigned short		uint16_t;	/* 符号無し16ビット整数 */
typedef signed int			int32_t;	/* 符号付き32ビット整数 */
typedef unsigned int		uint32_t;	/* 符号無し32ビット整数 */
typedef int					bool_t;		/* 真偽値 */

extern bool_t can232_open(void);
extern bool_t can232_send(uint16_t can_id, uint8_t can_length, uint8_t *pcan_data);
extern bool_t can232_receive(uint16_t *pcan_id, uint8_t *pcan_length, uint8_t *pcan_data);

/*
 *  制御周期
 */
#define T_T     0.01f       /*制御周期*/

/*
 *    ロータリエンコーダのカウント値と車輪の回転角度の変換係数    K_C
 *    ギヤボックスの減速比 G=29.8　ロータリエンコーダの分解能 P=24.0 [pulse/rev]
 *    ロータリエンコーダのカウント値 = COUNTとするとき、
 *    モータ軸の回転角度[radian]=2*PI*(float)COUNT/P
 *    車輪の回転角度[radian]=2*PI*(float)COUNT/P/G
 *    K_C = 2*PI/P/G = 0.008785214f
 */
#define K_C     0.008785214f

/*
 *    ジャイロセンサAD変換値から角加速度への変換係数        G_C
 *    ENC-03Mの分解能　0.67[mV/deg/sec]→0.383881723[V/rad/sec]
 *    1[V]->2.604969033[rad/sec]
 *    G_C=2.604969033*5/1024=0.012719575f    定義と出力の正負が違うのでマイナスをかける
 */
#define G_C    -0.012719575f

/*
 * 共有変数
 *   dTheta_1, ReCount_1 は，次の制御周期で値を使うためにグローバルとしている．
 *   その他は外部から制御状態を観測するためにグローバルにしている．
 */
static float       dTheta;            /*車体角速度*/
static float       dFai;              /*車輪角速度*/
static float       ThetaG;            /*車体姿勢角(角速度の積分)*/
static float       dTheta_1;          /*車体角速度の1周期前の値*/
static long        ReCount_1;         /*ロータリーエンコーダ値の1周期前の値 */

/*
 *  intの絶対値を返す．
 */
static inline int absf(int num)
{
    if(num<0) num=-num;
    return num;
}

/*
 *  ロータリーエンコーダ値とジャイロの出力から
 *  現在の状態を計算
 *  入力
 *    引数
 *    re_count : ロータリーエンコーダ値
 *    gyro     : ジャイロ値オフセット値は引いた値
 *  出力
 *    共有変数
 *    dTheta :  車体傾斜角速度
 *    ThetaG :  車体傾斜角
 *    dFai   :  車輪回転角速度
 */
static void current_status(long re_count, int gyro)
{
    if(absf(gyro) <= 1) gyro=0;                        /*ジャイロセンサの微小出力を無視*/

    dTheta = G_C*(float)gyro;                          /*車体の角速度を算出*/
    ThetaG += T_T / 2.0f * ( dTheta_1 + dTheta);       /*角速度を台形法により積分し姿勢角を算出*/
    dFai = (float)(re_count-ReCount_1)*K_C/T_T+dTheta; /*車輪の角速度を車輪の回転角を時間微分し算出*/
    ReCount_1 = re_count;                              /*ロータリエンコーダの値をセーブﾞ*/
    dTheta_1 = dTheta;                                 /*車体角速度の値をセーブﾞ*/
}

/*
 * 初期化
 */
static void init_current_satus(void)
{
    dTheta_1 = 0.0f;
    ReCount_1 = 0;
}

/*
 *  倒立制御
 */

/*
 *    モータのトルク定数
 *    モータ単体のトルク定数(0.001946)*ギヤ比(29.8)キヤボックスの効率(0.76)
 */
#define K_MOTOR    0.04408f

/*
 *    移動距離とカウントﾄ数の変換係数   C_C
 *    ロータリーエンコーダ分解能        P    24
 *    ギヤ比                            G    29.8
 *    車輪半径                          r    0.029
 *    1m移動したときのカウント数        C_C = (G*P)/(2*PI*r)
 */
#define C_C 3925.090183

#define F1     0.6229222193728826f        /*車体姿勢角に対するゲイン*/
#define F2     0.10131742697675375f       /*車体角速度に対するゲイン*/
#define F3     0.007264824512460763f      /*車輪角速度に対するゲイン*/
#define F4    -7.523680196085456E-5f      /*積算誤差に対するゲイン*/
#define F5    -0.16368035078682325f       /*前時間の制御入力に対するゲイン*/

/*
 * 共有変数
 */
static float    Tau_1;         /*前制御周期のトルク指令値*/
static float    OmegaD;        /*車輪回転角速度の目標値*/
static float    OmegaSum;      /*車輪回転角速度の制御偏差の積算値*/
static float    CountSum;      /*位置制御用の制御偏差の積算値*/

/*
 *  制御モード関連
 */

/*
 *  制御モード
 *  '1':位置制御，'0':速度制御
 */
static unsigned char ControlType;

/*
 *  位置制御の場合の制御位置
 */
static long ControlPosition;

/*
 * 速度制御モードの場合の速度
 * 前進の場合は正の値，後退の場合は負の値．
 *   1.0 = 1rad/sec
 * となる
 */
static float ControlSpeed;

/*
 * 制御入力計算用変数の初期化
 */
static void init_handstand_calc()
{
    init_current_satus();
    Tau_1  =0.0f;
    ThetaG = 0.0f;
    OmegaD = 0.0f;
    OmegaSum = 0.0f;
    CountSum = 0.0f;
    ControlType  = 1;
    ControlSpeed = 0.0f;
    ControlPosition = 0;
}

/*
 * 制御入力計算
 *  入力
 *    引数
 *    re_count  : ロータリーエンコーダ値
 *    gyro      : ジャイロの出力
 *    ctrl_type : 制御モード
 *                '1' : 位置制御モード
 *                '0' : 速度制御モード
 *    control_value : 制御値
 *  出力
 *    戻り値
 *    i_d 電流の目標値
 */
static float handstand_calc(long re_count, int gyro)
{
	float        tau;                /*トルク指令値*/
	float        omega_err;          /*車輪回転角速度の制御偏差*/
	float        count_err;          /*位置制御用の制御偏差*/

	/* 現在状態を計算 */
	current_status(re_count, gyro);

	/* 制御偏差算出 */
	omega_err = OmegaD - dFai;
	/* 制御入力算出 */
	tau = F1*ThetaG+F2*dTheta+F3*dFai+F4*OmegaSum+F5*Tau_1;
	/* 制御入力(トルク)をセーブ */
	Tau_1 = tau;
	/* 制御偏差を積算 */
	OmegaSum += omega_err;

	if(ControlType == 1){
		/* 位置制御モード */
		count_err = (float)re_count-(float)ControlPosition;/*ロータリエンコーダのカウント値から車輪角速度目標値を計算*/
		OmegaD = -0.0035f*count_err-0.00001f*CountSum;
		CountSum+=count_err;
		if(OmegaD < -15.0f) OmegaD = -15.0f;    /*出力制限*/
		if(OmegaD > 15.0f) OmegaD  =  15.0f;
	}
	else {
		/* 速度制御モード */
		OmegaD = ControlSpeed;    /*ユーザ指令値をそのまま車輪速度目標値に設定*/
	}

	/* モータに流す電流の目標値を返す */
	return tau/K_MOTOR;
}

/*
 * CANにより、float型のデータを送受信するための変換関数
 */
static int32_t ftox(float f)
{
	union{
		int32_t i;
		float f;
	} te;

	te.f = f;

	return te.i;
}

/*
 * IDの機能割り当てビットのマスク
 */
#define ID_FUNC_MASK      0x0f

/*
 * 各メッセージの機能割り当てビットの値
 */
#define ID_FUNC_START_REQ 0x01
#define ID_FUNC_AGND_GYRO 0x02
#define ID_FUNC_START_ACK 0x03
#define ID_FUNC_ID        0x04

/*
 *  IDの上位ビットの設定
 */
#define ID_UPPER_BIT_MASK 0x700
#define ID_UPPER_BIT      0x100

/*
 * IDのビット5,4に割り付ける固有IDのビットパターン
 * 起動時にディップスイッチの2,1の値を反映させる．
 */
#define IDENT_ID_BIT_MASK 0x0030

/*
 * 識別ID
 */
static uint16_t  IdentId;

/*
 * 受信に使用するスロットの番号
 */
#define RECV_SLOT_NO 1

/*
 *  識別IDの初期化，ポートの初期化後，ディップスイッチの状態が
 *  読めるようになってから呼び出すこと．
 */
static void ident_id_init(void)
{
	IdentId = ID_UPPER_BIT;
}

/*
 * 状態値メッセージの受信
 * 指定された時間で受信しなかった場合は，-1を返す．
 * 受信した場合は0を返す
 */
static int recv_state_message(uint8_t  *can_data, int timeout)
{
	uint16_t can_id;
	uint8_t  can_length;

	/*
	 *  CANからデータを受け取る
	 */
	can232_receive(&can_id, &can_length, can_data);

	if (can_id == (IdentId|ID_FUNC_AGND_GYRO)) {
		return 0;
	}

	return -1;
}

/*
 * CANによる接続と制御値計算
 */
static void acrobat_can_calc(void)
{
	uint16_t can_id;
	uint8_t  can_length;
	uint8_t  can_data[8];
	float  i_d;
	int32_t  re_count;
	int16_t  gyro;

	/*
	 * 制御値計算のための初期化
	 */
	init_handstand_calc();

	/*
	 * スタート要求を待つ
	 */
	while(1){
		/* データが送られて来るまで待つ */
		can232_receive(&can_id, &can_length, can_data);

		/* スタート要求なら */
		if(can_id == (IdentId|ID_FUNC_START_REQ)){
			/* ACK を返す */
			can232_send((IdentId|ID_FUNC_START_ACK), 0, can_data);
			break;
		}
	}

	/*
	 * 状態値メッセージが送られて来るのを待つ
	 *   PUPPY側は，開始をLEDでユーザーに通知するため，
	 *   タイムアウトは10秒とする
	 */
	if(recv_state_message(can_data, 10000) == -1){
		/* 10秒間メッセージが来なければタイムアウト */
		return;
	}

	/*
	*  CANから状態値メッセージを受け取り，制御値を求めCANに流す
	*/
	while(1){
		/* データの取り出し */
		re_count = *(int32_t*)(&can_data[0]);
		gyro     = *(int16_t*)(&can_data[4]);

		/* 制御値計算 */
		i_d = handstand_calc(re_count, gyro);

		/* 計算結果を送信するため変換 */
		*((int32_t*)can_data) = ftox(i_d);

		/* 送信 */
		can232_send((IdentId|ID_FUNC_ID), 4, can_data);

		if(recv_state_message(can_data, 1000) == -1){
			/* 1秒間メッセージが来なければタイムアウト */
			return;
		}
	}
}

extern void uart_init(void);
extern void serial_wri_dat(const char *buf, uint32_t len);

int main(void)
{
	uart_init();

	serial_wri_dat("start main\n", 11);

	ident_id_init();

	can232_open();

	while(1){
		acrobat_can_calc();
	}

	return 0;
}
