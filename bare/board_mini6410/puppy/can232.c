typedef unsigned char	uint8_t;	/* 符号無し8ビット整数 */
typedef unsigned short	uint16_t;	/* 符号無し16ビット整数 */
typedef unsigned int	uint32_t;	/* 符号無し32ビット整数 */
typedef int				bool_t;		/* 真偽値 */
typedef unsigned char	char_t;		/* 符号無しの文字型 */
typedef unsigned int	uint_t;		/* 自然なサイズの符号無し整数 */
#define true		1			/* 真 */
#define false		0			/* 偽 */

typedef struct {
	volatile unsigned long	ULCON;
	volatile unsigned long	UCON;
	volatile unsigned long	UFCON;
	volatile unsigned long	UMCON;
	volatile unsigned long	UTRSTAT;
	volatile unsigned long	UERSTAT;
	volatile unsigned long	UFSTAT;
	volatile unsigned long	UMSTAT;
	volatile unsigned char	UTXH;
	volatile unsigned char	res1[3];
	volatile unsigned char	URXH;
	volatile unsigned char	res2[3];
	volatile unsigned long	UBRDIV;
	volatile unsigned long	UDIVSLOT;
} S3C64XX_UART;

extern void serial_wri_dat(const char *buf, uint32_t len);
extern void serial_rea_dat(char *buf, uint32_t len);

static void can232_uart_init(void)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;
	
	uart->UFCON = 0;   // FIFO disabled
	uart->UMCON = 0;   // RTS AFC disabled
	uart->ULCON = 0x3; // 8-N-1
	uart->UCON = 0xF05; // 0xe45;  // polling mode - external clock
	uart->UBRDIV = 106; // integer part of DIV_VAL => (66MHz / (38400 x 16)) ‐ 1 = 106.42
	uart->UDIVSLOT = 0x54AA; // 0.42 * 16 = number of 1s in UDIVSLOT = 7
}

static void can232_serial_wri_chr(char_t c)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;
	
	while (!(uart->UTRSTAT & 0x2));
	uart->UTXH = c;
}

static char_t can232_serial_rea_chr(void)
{
	volatile S3C64XX_UART *const uart =  (S3C64XX_UART *)0x7F005C00;
	
	while (!(uart->UTRSTAT & 0x1) && !(uart->UERSTAT & 0x3));
	return uart->URXH & 0xff;
}

static void can232_serial_wri_dat(const char_t *buf, uint32_t len)
{
	while (len > 0) {
		can232_serial_wri_chr(*buf++);
		len--;
	}
}

static void can232_serial_rea_dat(char_t *buf, uint32_t len)
{
	while (len > 0) {
		*buf = can232_serial_rea_chr();
		buf++;
		len--;
	}
}

/*
 *  CAN232のオープン
 */
bool_t can232_open(void)
{
	char_t c[10];

	can232_uart_init();

	/*
	 *  クローズ
	 */
	can232_serial_wri_dat((char_t *)"C\r", 2);
	can232_serial_rea_dat(c, 1);

	/*
	 *  スピードの設定
	 */
	can232_serial_wri_dat((char_t *)"S6\r", 3);
	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r') {
		serial_wri_dat("ERROR speed\n", 12);
		return false;
	}

	/*
	 *  オープン
	 */
	can232_serial_wri_dat((char_t *)"O\r", 2);
	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r') {
		serial_wri_dat("ERROR open", 10);
		return false;
	}

	return true;
}

static uint_t str2hex(uint8_t *pstr, uint_t length)
{
	int i;
	uint_t val = 0;
	
	for(i = 0; i < length; i++) {
		if ( pstr[i] >= '0' && pstr[i] <= '9') {
			val +=  (pstr[i] - '0') << ((length - i -1) * 4);
		}else{
			val +=  (0xa + (pstr[i] - 'A')) << ((length - i -1) * 4);
		}
	}

	return val;
}

static int Phase;
static int Phase1;

bool_t can232_receive(uint16_t *pcan_id, uint8_t *pcan_length, uint8_t *pcan_data)
{
	char_t  c[20];
	int     i;

    Phase = 0;
    Phase1 = 0;
	do{
		can232_serial_rea_dat(c, 1);
		/* 't'が来るのを待つ */
        Phase1++;
	} while (c[0] != 't');
    Phase = 1;
	/* ID を取得 */
	can232_serial_rea_dat(c, 3);
	*pcan_id = str2hex(c, 3);
    Phase = 2;
	/* Length を取得 */
	can232_serial_rea_dat(c, 1);
	*pcan_length = str2hex(c, 1);

    Phase = 3;
	can232_serial_rea_dat(c, *pcan_length*2);

	for(i = 0; i < *pcan_length; i++){
		pcan_data[i] = str2hex(&c[i * 2], 2);
	}
    Phase = 4;
	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r')  {
		serial_wri_dat("ERROR receive\n", 14);
		return false;
	}
    Phase = 5;

	return true;
}

static void hex2str(uint8_t *pstr, uint_t hex, uint_t length)
{
	int i;

	for(i = length - 1; i >= 0; i--) {
		if ((hex & 0x0f) >= 0 && (hex & 0x0f) <= 9) {
			pstr[i] = '0' + (hex & 0x0f);
		}
		else {
			pstr[i] = 'A' + ((hex & 0x0f) - 0xA);
		}
		hex = hex >> 4;
	}
}

bool_t can232_send(uint16_t can_id, uint8_t can_length, uint8_t *pcan_data)
{
	uint8_t str_can_id[3];
	uint8_t str_can_length[1];
	uint8_t str_can_data[16];
	char_t  c[2];
	int     i;

	hex2str(str_can_id, can_id, 3);
	hex2str(str_can_length, can_length, 1);
	for(i = 0; i < can_length; i++){
		hex2str(&str_can_data[i*2], pcan_data[i], 2);
	}

    Phase = 6;
	can232_serial_wri_dat((char_t *)"t", 1);
    Phase = 7;
	can232_serial_wri_dat((char_t *)str_can_id, 3);
    Phase = 8;
	can232_serial_wri_dat((char_t *)str_can_length, 1);
    Phase = 9;
	can232_serial_wri_dat((char_t *)str_can_data, can_length * 2);
    Phase = 10;
	can232_serial_wri_dat((char_t *)"\r", 1);
    Phase = 11;
	can232_serial_rea_dat(c, 1);
	if (c[0] != 'z')  {
		return false;
	}

	can232_serial_rea_dat(c, 1);
	if (c[0] != '\r')  {
		serial_wri_dat("ERROR send\n", 11);
		return false;
	}

	return true;
}
