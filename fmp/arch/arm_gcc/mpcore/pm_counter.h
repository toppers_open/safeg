#ifndef PM_COUNTER_H
#define PM_COUNTER_H

// Performance Monitor用マクロ
#define CP15_PM_CONTROL_READ(x) Asm("mrc p15, 0, %0, c9, c12, 0":"=r"(x))
#define CP15_PM_CONTROL_WRITE(x) Asm("mcr p15, 0, %0, c9, c12, 0"::"r"(x))

#define CP15_PM_COUNT_ENABLE_SET_READ(x) Asm("mrc p15, 0, %0, c9, c12, 1":"=r"(x))
#define CP15_PM_COUNT_ENABLE_SET_WRITE(x) Asm("mcr p15, 0, %0, c9, c12, 1"::"r"(x))

#define CP15_PM_COUNT_ENABLE_CLEAR_READ(x) Asm("mrc p15, 0, %0, c9, c12, 2":"=r"(x))
#define CP15_PM_COUNT_ENABLE_CLEAR_WRITE(x) Asm("mcr p15, 0, %0, c9, c12, 2"::"r"(x))

#define CP15_PM_CYCLE_READ(x) Asm("mrc p15, 0, %0, c9, c13, 0":"=r"(x))
#define CP15_PM_CYCLE_WRITE(x) Asm("mcr p15, 0, %0, c9, c13, 0"::"r"(x))

#define CP15_PM_CYCLE_READ(x) Asm("mrc p15, 0, %0, c9, c13, 0":"=r"(x))
#define CP15_PM_CYCLE_WRITE(x) Asm("mcr p15, 0, %0, c9, c13, 0"::"r"(x))

#define CP15_PM_EVENT_COUNTER_SELECT_READ(x) Asm("mrc p15, 0, %0, c9, c12, 5":"=r"(x))
#define CP15_PM_EVENT_COUNTER_SELECT_WRITE(x) Asm("mcr p15, 0, %0, c9, c12, 5"::"r"(x))

#define CP15_PM_EVENT_TYPE_SELECT_READ(x) Asm("mrc p15, 0, %0, c9, c13, 1":"=r"(x))
#define CP15_PM_EVENT_TYPE_SELECT_WRITE(x) Asm("mcr p15, 0, %0, c9, c13, 1"::"r"(x))

/* Performance Monitors Cycle Counter */
#define CP15_PMCCNTR_WRITE(x)    Asm("mcr p15, 0, %0, c9, c13, 0"::"r"(x))
#define CP15_PMCCNTR_READ(x)     Asm("mrc p15, 0, %0, c9, c13, 0":"=r"(x))

/* Performance Monitors Control Register */
#define CP15_PMCR_WRITE(x)  Asm("mcr p15, 0, %0, c9, c12, 0":: "r"(x))
#define CP15_PMCR_READ(x)   Asm("mrc p15, 0, %0, c9, c12, 0":"=r"(x))

#define CP15_PMCR_PMCCNTR_D      0x08U
#define CP15_PMCR_PMCCNTR_CLEAR  0x04U
#define CP15_PMCR_ALLCNTR_ENABLE 0x01U

#define CP15_PMCNTENSET_WRITE(x)  Asm("mcr p15, 0, %0, c9, c12, 1":: "r"(x))
#define CP15_PMCNTENSET_READ(x)   Asm("mrc p15, 0, %0, c9, c12, 1":"=r"(x))

#define CP15_PMCNTENSET_CCNTR_ENABLE 0x80000000U

// 64分周タイマの使用
#define TIMER_64DIV

/*
 * パフォーマンスカウンタの開始
 */
Inline unsigned long
pmon_start_cycle_counter(void)
{
#ifdef TIMER_64DIV
    unsigned long x;

    x = 1 << 31;
    asm volatile("mcr   p15, 0, %0, c9, c12, 1" :: "r" (x));

    asm volatile("mrc   p15, 0, %0, c9, c12, 0" : "=r" (x));
    x |= ((1 << 3) | (1 << 2) | (1 << 0));
    asm volatile("mcr   p15, 0, %0, c9, c12, 0" :: "r" (x));

    asm volatile("mrc   p15, 0, %0, c9, c13, 0" : "=r" (x));
#else
    CP15_PM_EVENT_COUNTER_SELECT_WRITE(0);
    CP15_PM_EVENT_TYPE_SELECT_WRITE(0x11);
    CP15_PM_COUNT_ENABLE_CLEAR_WRITE((1<<31)); // disable cycle counter
    CP15_PM_CONTROL_WRITE(0); // disable all counters + count in cycles
    CP15_PM_CYCLE_WRITE(0);   // load 0 to counter
    CP15_PM_COUNT_ENABLE_SET_WRITE((1<<31)); // enable cycle counter
    CP15_PM_CONTROL_WRITE(1); // enable all counters
#endif

    return 0;
}

/*
 * パフォーマンスカウンタの停止と値読み取り
 */
Inline unsigned long
pmon_read_cycle_counter(void)
{
    unsigned long x;
    // cycle read
    asm volatile ("mrc  p15, 0, %0, c9, c13, 0": "=r" (x));
    CP15_PM_COUNT_ENABLE_CLEAR_WRITE((1<<31)); // disable cycle counter
    CP15_PM_CONTROL_WRITE(0); // disable all counters + count in cycles
    return x;
}

/*
 *  パフォーマンスカウンタの読み込み
 */
Inline void 
x_get_pcc(uint32_t *p_count)
{
	CP15_PMCCNTR_READ(*p_count);
}

/*
 *  パフォーマンスカウンタのスタート
 */
Inline void
pmon_start(void)
{
	uint32_t tmp;
	CP15_PMCR_READ(tmp);
	CP15_PMCR_WRITE(tmp|CP15_PMCR_ALLCNTR_ENABLE);
	CP15_PMCNTENSET_WRITE(tmp|CP15_PMCNTENSET_CCNTR_ENABLE);
}

/*
 *  パフォーマンスカウンタのストップ
 */
Inline void
pmon_stop(void)
{
	uint32_t tmp;
	CP15_PMCR_READ(tmp);
    
	CP15_PMCR_WRITE(tmp&(~CP15_PMCR_ALLCNTR_ENABLE));
	CP15_PMCNTENSET_WRITE(tmp&(~CP15_PMCNTENSET_CCNTR_ENABLE));
}

#endif /* PM_COUNTER_H */
