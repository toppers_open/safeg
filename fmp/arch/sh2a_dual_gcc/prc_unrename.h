/* This file is generated from prc_rename.def by genrename. */

/* This file is included only when prc_rename.h has been included. */
#ifdef TOPPERS_PRC_RENAME_H
#undef TOPPERS_PRC_RENAME_H

/*
 *  prc_config.c, prc_support.S
 */
#undef prc_mprc_initialize
#undef prc_initialize
#undef prc_exit
#undef int_iipm_tbl
#undef ipr_info_tbl
#undef x_config_int
#undef iipm_disall
#undef int_entry
#undef exc_entry
#undef default_exc_handler
#undef default_exc_handler_entry
#undef default_int_handler
#undef default_int_handler_entry
#undef start_r

/*
 *  kernel_cfg.c
 */
#undef p_pcb_table
#undef p_vectors_table


#ifdef TOPPERS_LABEL_ASM

/*
 *  prc_config.c, prc_support.S
 */
#undef _prc_mprc_initialize
#undef _prc_initialize
#undef _prc_exit
#undef _int_iipm_tbl
#undef _ipr_info_tbl
#undef _x_config_int
#undef _iipm_disall
#undef _int_entry
#undef _exc_entry
#undef _default_exc_handler
#undef _default_exc_handler_entry
#undef _default_int_handler
#undef _default_int_handler_entry
#undef _start_r

/*
 *  kernel_cfg.c
 */
#undef _p_pcb_table
#undef _p_vectors_table


#endif /* TOPPERS_LABEL_ASM */


#endif /* TOPPERS_PRC_RENAME_H */
