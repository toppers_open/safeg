
                         Test examples for SafeG
                         =======================

本ディレクトリにはSafeG向けFMPカーネルのサンプルプログラムが含まれている．

【ディレクトリ構造とプログラムの種類】
・obj/obj_safeg_xxx/trust
    safeg向けサンプルプログラム(Trust FMP)
・obj/obj_safeg_xxx/non_trust
    safeg向けサンプルプログラム(Non-Trust FMP)


【ビルド方法】
サンプルプログラムのビルドは通常のFMPカーネル同様に行う．
具体的には以下のように行う．
$ make depend
$ make

これにより，makeを実行したディレクトリに実行ファイルfmp.axfが作成される．
(拡張子はMakefileによって指定可能なため，プログラムにより実行ファイルの拡
張子が異なる可能性がある)


【クリーン方法】
以下のコマンドを実行する．
$ make clean


【SafeG非対応のFMPカーネルをビルドする方法】
本サンプルプログラムをビルドしてできるトラスト側FMPカーネルはSafeGを用いて
実行することを前提としている．SafeGを使用せず，トラスト側FMPカーネルのみを
実行したい場合には，以下のようにmakeにマクロを指定する．
$ make SAFEG=disable

参照：target/kzm_ca9t_gcc/Makefile.target

ノントラスト側FMPカーネルでは，通常通りmakeしたものでSafeG使用/未使用時どち
らでも実行することが可能である．
(トラスト側ではSafeGを用いて別のOSと実行する場合にTrustZoneの設定を行う必要
があるが，ノントラスト側ではそうした必要がないため)


以上
