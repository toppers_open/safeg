#include <kernel.h>
#include <t_syslog.h>
#include <t_stdlib.h>
#include "syssvc/serial.h"
#include "syssvc/syslog.h"
#include "kernel_cfg.h"
#include "sample1.h"
#include "safeg_syscalls.h"
#include "kzm_ca9.h"
#include "target_timer.h"

int mig_flag = 0;
int busy_flag = 0;
int rot_flag = 0;

// CPU ID取得用マクロ
#define CP15_MPIDR_READ(x)                                  \
    __asm__ volatile ("mrc p15, 0, %0, c0, c0, 5":"=r"(x));

Inline void
svc_perror(const char *file, int_t line, const char *expr, ER ercd)
{
	if (ercd < 0) {
		t_perror(LOG_ERROR, file, line, expr, ercd);
	}
}

#define	SVC_PERROR(expr)	svc_perror(__FILE__, __LINE__, #expr, (expr))

void task(intptr_t exinf)
{
	int_t	i;
	static int_t	n = 0;
	int_t		tskno = (int_t) exinf;
	const char	*graph[] = { "|", "  +", "    *", "     #" };

    int current_cpu;
    CP15_MPIDR_READ(current_cpu);
    current_cpu &= 0x0f;
    // FMPカーネルのコア表記に変更
    current_cpu++;

    if(mig_flag != current_cpu){
        if(mig_flag == 1){
            //syslog(LOG_NOTICE, "TASK1_1 : mig_tsk(TSK_SELF, 1);");
            SVC_PERROR(mig_tsk(TSK_SELF, 1));
            current_cpu = 1;
        }else if(mig_flag == 2){
            //syslog(LOG_NOTICE, "TASK1_1 : mig_tsk(TSK_SELF, 2);");
            SVC_PERROR(mig_tsk(TSK_SELF, 2));
            current_cpu = 2;
        }else if(mig_flag == 3){
            //syslog(LOG_NOTICE, "TASK1_1 : mig_tsk(TSK_SELF, 3);");
            SVC_PERROR(mig_tsk(TSK_SELF, 3));
            current_cpu = 3;
        }else if(mig_flag == 4){
            //syslog(LOG_NOTICE, "TASK1_1 : mig_tsk(TSK_SELF, 4);");
            SVC_PERROR(mig_tsk(TSK_SELF, 4));
            current_cpu = 4;
        }
    }

	syslog(LOG_NOTICE, "task%d is running on [CPU%d] (%03d).   %s",
           tskno, current_cpu, ++n, graph[tskno-1]);

    if(busy_flag){
        for(i=0; i<1000; i++);
    }
}

void static_task(intptr_t exinf)
{
	int_t	i;
	static int_t	n = 0;
	int_t		tskno = (int_t) exinf;
	const char	*graph[] = { "|", "  +", "    *", "     #" };

    int current_cpu;
    CP15_MPIDR_READ(current_cpu);
    current_cpu &= 0x0f;
    // FMPカーネルのコア表記に変更
    current_cpu++;

    while(1);
    //	syslog(LOG_NOTICE, "task%d is running on [CPU%d] (%03d).   %s",
    //           tskno, current_cpu, ++n, graph[tskno-1]);
}

void btask(intptr_t exinf)
{
	volatile struct safeg_smc_args ret_args;
	
    while(1){
        safeg_switch_to_nt(&ret_args);
        /* Linuxからリターンした場合 */
        syslog(LOG_NOTICE, "btask%d : GPOS reply!", exinf);
        syslog(LOG_NOTICE, "    ret_args.arg0 = %d", ret_args.arg0);
        syslog(LOG_NOTICE, "    ret_args.arg1 = %d", ret_args.arg1);
    }
}

void cyclic_handler(intptr_t exinf)
{
    if(rot_flag){
        if(mig_flag == 4)
            mig_flag = 1;
        else
            mig_flag++;
    }
	SVC_PERROR(iact_tsk(TASK1_1));
}

void main_task(intptr_t exinf)
{
	ER_UINT	ercd;
    char_t c;

#ifdef DBG_VERBOSE
	SVC_PERROR(syslog_msk_log(0, LOG_UPTO(LOG_DEBUG)));
#else
	SVC_PERROR(syslog_msk_log(LOG_UPTO(LOG_INFO), LOG_UPTO(LOG_EMERG)));
#endif
	syslog(LOG_NOTICE, "Sample program starts (exinf = %d).", (int_t) exinf);

	ercd = serial_opn_por(TASK_PORTID);
	if (ercd < 0 && MERCD(ercd) != E_OBJ) {
		syslog(LOG_ERROR, "%s (%d) reported by `serial_opn_por'.",
									itron_strerror(ercd), SERCD(ercd));
	}
	SVC_PERROR(serial_ctl_por(TASK_PORTID,
							(IOCTL_CRLF | IOCTL_FCSND | IOCTL_FCRCV)));		
					
	SVC_PERROR(dis_int(DIC_IRQNO_WDT));

    syslog(LOG_NOTICE, "main_task > timer is disabled.");
    syslog(LOG_NOTICE, "    To enable timer, press [e]");

    while(1){
        SVC_PERROR(serial_rea_dat(TASK_PORTID, &c, 1));
        //syslog(LOG_NOTICE, "\nread data = %c", c);
        if(c == 'a') {
            SVC_PERROR(act_tsk(TASK1_1));
        }else if(c == 'b') {
            busy_flag = 1;
            syslog(LOG_NOTICE, ">> busy_flag is %d", busy_flag);
        }else if(c == 'x') {
            busy_flag = 0;
            syslog(LOG_NOTICE, ">> busy_flag is %d", busy_flag);
        }else if(c == 'e') {
            syslog(LOG_NOTICE, ">> timer interrupt ON");
            SVC_PERROR(ena_int(DIC_IRQNO_WDT));
        }else if(c == 'd') {
            syslog(LOG_NOTICE, ">> timer interrupt OFF");
            SVC_PERROR(dis_int(DIC_IRQNO_WDT));
        }else if(c == 'c') {
            syslog(LOG_NOTICE, ">> start cyclic handler!!");
            SVC_PERROR(sta_cyc(CYCHDR1));
        }else if(c == 's') {
            syslog(LOG_NOTICE, ">> stop cyclic handler!!");
            SVC_PERROR(stp_cyc(CYCHDR1));
        }else if(c == 'r') {
            if(!rot_flag){
                syslog(LOG_NOTICE, ">> task rotation start!!");
                rot_flag = 1;
            }else{
                syslog(LOG_NOTICE, ">> task rotation stop!!");
                rot_flag = 0;
            }
        }else if(c == '1') {
            mig_flag = 1;
            //syslog(LOG_NOTICE,">> mig_tsk(TASK1_1, 1)");
            //SVC_PERROR(mig_tsk(TASK1_1, 1));
        }else if(c == '2') {
            mig_flag = 2;
        }else if(c == '3') {
            mig_flag = 3;
        }else if(c == '4') {
            mig_flag = 4;
        }else if(c == '5') {
            SVC_PERROR(act_tsk(TASK2_1));
        }else if(c == '6') {
            SVC_PERROR(act_tsk(TASK3_1));
        }else if(c == '7') {
            SVC_PERROR(act_tsk(TASK4_1));
        }
    }
    
	syslog(LOG_NOTICE, "Sample program ends.");
	SVC_PERROR(ext_ker());
	assert(0);
}
