#include <kernel.h>
#include <t_syslog.h>
#include <t_stdlib.h>
#include "syssvc/serial.h"
#include "syssvc/syslog.h"
#include "kernel_cfg.h"
#include "sample1.h"
#include "kzm_ca9.h"
#include "target_timer.h"

// CPU ID取得用マクロ
#define CP15_MPIDR_READ(x)                                  \
    __asm__ volatile ("mrc p15, 0, %0, c0, c0, 5":"=r"(x));

Inline void
svc_perror(const char *file, int_t line, const char *expr, ER ercd)
{
	if (ercd < 0) {
		t_perror(LOG_ERROR, file, line, expr, ercd);
	}
}

#define SVC_PERROR(expr)    svc_perror(__FILE__, __LINE__, #expr, (expr))

/*
 *  CPU例外ハンドラ
 */
void
cpuexc_handler_undef(void *p_excinf)
{
	ID		tskid;
    int i;

    int cpu_id;
    CP15_MPIDR_READ(cpu_id);
    cpu_id &= 0x0f;
    
	syslog(LOG_NOTICE, "(Processor %d) UNDEF exception handler1 (p_excinf = %08p).", cpu_id+1, p_excinf);

    for(i=0; i<10; i++){
        syslog(LOG_NOTICE, " (p_excinf[%d] = 0x%X).", i, ((uint32_t*)(p_excinf))[i]);
    }

	if (sns_ctx() != true) {
		syslog(LOG_WARNING,
					"sns_ctx() is not true in CPU exception handler.");
	}
	if (sns_dpn() != true) {
		syslog(LOG_WARNING,
					"sns_dpn() is not true in CPU exception handler.");
	}
	syslog(LOG_INFO, "sns_loc = %d sns_dsp = %d sns_tex = %d",
									sns_loc(), sns_dsp(), sns_tex());

	syslog(LOG_INFO, "xsns_dpn = %d xsns_xpn = %d",
									xsns_dpn(p_excinf), xsns_xpn(p_excinf));

    
	if (xsns_xpn(p_excinf)) {
		syslog(LOG_NOTICE, "Sample program ends with exception.");
		SVC_PERROR(ext_ker());
		assert(0);
	}

	SVC_PERROR(iget_tid(&tskid));
	SVC_PERROR(iras_tex(tskid, 0x8000U));
}

void
cpuexc_handler_swi(void *p_excinf)
{
	ID		tskid;
    int i;

    int cpu_id;
    CP15_MPIDR_READ(cpu_id);
    cpu_id &= 0x0f;
    
	syslog(LOG_NOTICE, "(Processor %d) SWI exception handler1 (p_excinf = %08p).", cpu_id+1, p_excinf);

    for(i=0; i<10; i++){
        syslog(LOG_NOTICE, " (p_excinf[%d] = 0x%X).", i, ((uint32_t*)(p_excinf))[i]);
    }

	if (sns_ctx() != true) {
		syslog(LOG_WARNING,
					"sns_ctx() is not true in CPU exception handler.");
	}
	if (sns_dpn() != true) {
		syslog(LOG_WARNING,
					"sns_dpn() is not true in CPU exception handler.");
	}
	syslog(LOG_INFO, "sns_loc = %d sns_dsp = %d sns_tex = %d",
									sns_loc(), sns_dsp(), sns_tex());

	syslog(LOG_INFO, "xsns_dpn = %d xsns_xpn = %d",
									xsns_dpn(p_excinf), xsns_xpn(p_excinf));

    
	if (xsns_xpn(p_excinf)) {
		syslog(LOG_NOTICE, "Sample program ends with exception.");
		SVC_PERROR(ext_ker());
		assert(0);
	}

	SVC_PERROR(iget_tid(&tskid));
	SVC_PERROR(iras_tex(tskid, 0x8000U));
}

void
cpuexc_handler_pabort(void *p_excinf)
{
	ID		tskid;
    int i;

    int cpu_id;
    CP15_MPIDR_READ(cpu_id);
    cpu_id &= 0x0f;
    
	syslog(LOG_NOTICE, "(Processor %d) PABORT exception handler1 (p_excinf = %08p).", cpu_id+1, p_excinf);

    for(i=0; i<10; i++){
        syslog(LOG_NOTICE, " (p_excinf[%d] = 0x%X).", i, ((uint32_t*)(p_excinf))[i]);
    }

	if (sns_ctx() != true) {
		syslog(LOG_WARNING,
					"sns_ctx() is not true in CPU exception handler.");
	}
	if (sns_dpn() != true) {
		syslog(LOG_WARNING,
					"sns_dpn() is not true in CPU exception handler.");
	}
	syslog(LOG_INFO, "sns_loc = %d sns_dsp = %d sns_tex = %d",
									sns_loc(), sns_dsp(), sns_tex());

	syslog(LOG_INFO, "xsns_dpn = %d xsns_xpn = %d",
									xsns_dpn(p_excinf), xsns_xpn(p_excinf));

    
	if (xsns_xpn(p_excinf)) {
		syslog(LOG_NOTICE, "Sample program ends with exception.");
		SVC_PERROR(ext_ker());
		assert(0);
	}

	SVC_PERROR(iget_tid(&tskid));
	SVC_PERROR(iras_tex(tskid, 0x8000U));
}

void
cpuexc_handler_dabort(void *p_excinf)
{
	ID		tskid;
    int i;

    int cpu_id;
    CP15_MPIDR_READ(cpu_id);
    cpu_id &= 0x0f;
    
	syslog(LOG_NOTICE, "(Processor %d) DABORT exception handler1 (p_excinf = %08p).", cpu_id+1, p_excinf);

    for(i=0; i<10; i++){
        syslog(LOG_NOTICE, " (p_excinf[%d] = 0x%X).", i, ((uint32_t*)(p_excinf))[i]);
    }

	if (sns_ctx() != true) {
		syslog(LOG_WARNING,
					"sns_ctx() is not true in CPU exception handler.");
	}
	if (sns_dpn() != true) {
		syslog(LOG_WARNING,
					"sns_dpn() is not true in CPU exception handler.");
	}
	syslog(LOG_INFO, "sns_loc = %d sns_dsp = %d sns_tex = %d",
									sns_loc(), sns_dsp(), sns_tex());

	syslog(LOG_INFO, "xsns_dpn = %d xsns_xpn = %d",
									xsns_dpn(p_excinf), xsns_xpn(p_excinf));

    
	if (xsns_xpn(p_excinf)) {
		syslog(LOG_NOTICE, "Sample program ends with exception.");
		SVC_PERROR(ext_ker());
		assert(0);
	}

	SVC_PERROR(iget_tid(&tskid));
	SVC_PERROR(iras_tex(tskid, 0x8000U));
}

