/* CPU例外関連の定義 */
/* 例外の番号は、core_config.hで以下のように定義されている
EXCH_NO_RESET     0
EXCH_NO_UNDEF     1
EXCH_NO_SWI       2
EXCH_NO_PABORT    3
EXCH_NO_DABORT    4
EXCH_NO_IRQ       5
EXCH_NO_FIQ       6
*/
/* リセット */
#define CPUEXC1_SWI		(0x10000|EXCH_NO_SWI)
#define CPUEXC2_SWI		(0x20000|EXCH_NO_SWI)
#define CPUEXC3_SWI		(0x30000|EXCH_NO_SWI)
#define CPUEXC4_SWI		(0x40000|EXCH_NO_SWI)
/* 未定義 */
#define CPUEXC1_UNDEF		(0x10000|EXCH_NO_UNDEF)
#define CPUEXC2_UNDEF		(0x20000|EXCH_NO_UNDEF)
#define CPUEXC3_UNDEF		(0x30000|EXCH_NO_UNDEF)
#define CPUEXC4_UNDEF		(0x40000|EXCH_NO_UNDEF)
/* プリフェッチアボート */
#define CPUEXC1_PABORT		(0x10000|EXCH_NO_PABORT)
#define CPUEXC2_PABORT		(0x20000|EXCH_NO_PABORT)
#define CPUEXC3_PABORT		(0x30000|EXCH_NO_PABORT)
#define CPUEXC4_PABORT		(0x40000|EXCH_NO_PABORT)
/* データアボート */
#define CPUEXC1_DABORT		(0x10000|EXCH_NO_DABORT)
#define CPUEXC2_DABORT		(0x20000|EXCH_NO_DABORT)
#define CPUEXC3_DABORT		(0x30000|EXCH_NO_DABORT)
#define CPUEXC4_DABORT		(0x40000|EXCH_NO_DABORT)

#define RAISE_CPU_EXCEPTION	(*((volatile int *) 0xFFFFFEC1U))
#define CANNOT_RETURN_CPUEXC

#ifndef TOPPERS_MACRO_ONLY
extern void	cpuexc_handler_swi(void *p_excinf);
extern void	cpuexc_handler_undef(void *p_excinf);
extern void	cpuexc_handler_pabort(void *p_excinf);
extern void	cpuexc_handler_dabort(void *p_excinf);
#endif /* TOPPERS_MACRO_ONLY */
