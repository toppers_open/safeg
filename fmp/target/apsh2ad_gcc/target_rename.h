/* This file is generated from target_rename.def by genrename. */

#ifndef TOPPERS_TARGET_RENAME_H
#define TOPPERS_TARGET_RENAME_H

#define target_mprc_initialize		_kernel_target_mprc_initialize

#ifdef TOPPERS_LABEL_ASM

#define _target_mprc_initialize		__kernel_target_mprc_initialize

#endif /* TOPPERS_LABEL_ASM */

#include "sh2a_dual_gcc/prc_rename.h"

#endif /* TOPPERS_TARGET_RENAME_H */
