/* This file is generated from target_rename.def by genrename. */

/* This file is included only when target_rename.h has been included. */
#ifdef TOPPERS_TARGET_RENAME_H
#undef TOPPERS_TARGET_RENAME_H

#undef target_mprc_initialize

#ifdef TOPPERS_LABEL_ASM

#undef _target_mprc_initialize

#endif /* TOPPERS_LABEL_ASM */

#include "sh2a_dual_gcc/prc_unrename.h"

#endif /* TOPPERS_TARGET_RENAME_H */
