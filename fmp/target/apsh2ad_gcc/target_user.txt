		TOPPERS/FMPカーネル
		APSH2AD(GCC)ターゲット依存部 ユーザーズマニュアル

		対応バージョン: Release 1.1.1
		最終更新: 2010年5月11日

このドキュメントは，TOPPERS/FMPカーネルのAPSH2AD(GCC)ターゲット依存部を
使用するために必要な事項を説明するものである．
SH2A-DUALアーキテクチャ共通の事項は、asp/arch/sh2a_dual_gcc/prc_user.txt
に記載しているので、こちらも参照のこと。

----------------------------------------------------------------------
 TOPPERS/FMP Kernel
     Toyohashi Open Platform for Embedded Real-Time Systems/
     Advanced Standard Profile Kernel

 Copyright (C) 2008 by Embedded and Real-Time Systems Laboratory
             Graduate School of Information Science, Nagoya Univ., JAPAN
 Copyright (C) 2007-2010 by Industrial Technology Institute,
                                    Miyagi Prefectural Government, JAPAN
 
 上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
     権表示，この利用条件および下記の無保証規定が，そのままの形でソー
     スコード中に含まれていること．
 (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
     用できる形で再配布する場合には，再配布に伴うドキュメント（利用
     者マニュアルなど）に，上記の著作権表示，この利用条件および下記
     の無保証規定を掲載すること．
 (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
     用できない形で再配布する場合には，次のいずれかの条件を満たすこ
     と．
   (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
       作権表示，この利用条件および下記の無保証規定を掲載すること．
   (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
       報告すること．
 (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
     害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
     また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
     由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
     免責すること．
 
 本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 の責任を負わない．
 
----------------------------------------------------------------------


(1) 対応しているターゲットシステムの種類・構成
    ・ターゲットハードウェア（ボード等）の名称と対応している構成
    　　ボード：AP-SH2AD-0A（アルファプロジェクト製）
    　　プロセッサ：SH2A-DUAL（SH7205）
    ・ターゲット略称等
    　　・ターゲット略称：apsh2ad_gcc
	　　・システム略称：apsh2ad
	　　・開発環境略称：gcc
    ・プロセッサ，チップ，開発環境依存部の構成
		arch/
			sh2a_dual_gcc/	SH2A-DUAL(GCC)アーキテクチャ依存部
			gcc/			GCC開発環境依存部
			logtrace/		トレースログ記録のサンプルコード
    ・使用するPDIC
		pdic/
			sh/				SHシリーズ用SCIFドライバ
　　・依存している個別パッケージのバージョン番号
		APSH2AD(GCC)ターゲット依存部（バージョン 1.1.1）の個別パッケージ
		が依存している個別パッケージと，動作確認を行ったバージョンは次
		の通り．

	個別パッケージの名称	バージョン	個別パッケージファイル名
	------------------------------------------------------------------
	ターゲット非依存部		1.1.1		fmp-1.1.1.tar.gz

    ・サポートしているプロセッサ数
    　　２個

(2) 使用する開発環境と動作検証した条件（バージョン，オプション等）
    ・言語処理系（コンパイラ，アセンブラ，リンカ等）
　　　　GCC 4.1 で動作確認を行った．動作確認した GCC 4.1 は，以下の
　　　　サイトからバイナリパッケージをダウンロードして用いた．

        http://www.superh-tkernel.org/jpn/download/tools/index.html

　　・デバッグ環境
　　　　動作確認したデバッガは，ビットラン（株）製のCode Debugger DR-01
　　　　である．なお，JTAGデバッグ時は，RAM上にカーネルの
　　　　コードを置くため，ダウンロード前にRAMの初期化が必要となる．
　　　　RAMの初期化に関しては，DR-01に付属している
　　　　スクリプトファイル（ap_sh2ad_0a.job）を
　　　　用いた．

		・ボードの設定
			本カーネル動作時の AP-SH2AD-0A のディップスイッチの設定は
			以下の通りである．

		　・JTAGデバッグ時
			   SS1　　　: DEBUGモード
			   
			   SW1-1    : ON　　 CS0空間バス幅　16ビット
			   SW1-2,3  : ON,ON　クロック供給源 EXTAL/水晶発振子
			   　　　　　　　　　分周器1　1
			   　　　　　　　　　PLL回路　ON(12, 16)
			   　　　　　　　　　CKI0の周波数　（EXTAL/水晶）×４
			   　　　　　　　　　
			   SW1-4,5  : ON,ON　FlashROMライトプロテクト解除
			   SW1-6    : ON　　 ボード上のFlashROMを使用する
			   SW1-7    : ON　　 SDRAMを使用する
			   SW1-8    : ON

			 SW3の設定は、出荷時のままである。
			   SW3-1    : ON
			   SW3-2    : OFF
			   SW3-3    : ON
			   SW3-4    : ON
			   SW3-5    : OFF
			   SW3-6    : ON


(3) ターゲット定義事項の規定

(3-1) データ型に関する規定

ターゲット依存のデータ型のサイズは次の通り．
	int_t			32ビット
	long_t			32ビット
	char_t			8ビット
	int_least8_t	8ビット
	void *			32ビット
	intptr_t		32ビット

ターゲット依存のデータ型の有無は次の通り．

	int8_t，uint8_t			あり
	int64_t，uint64_t		あり
	int128_t，uint128_t		なし
	float32_t				あり
	double64_t				あり

(3-2) 割込み処理に関する規定
    ・割込みハンドラ番号と割込み番号の割当て，両者の対応
    　　プロセッサのベクタ番号をそのまま用いる。

    ・割込み優先度の段階数（TMIN_INTPRIの値），その設定方法
    　　デフォルトでは、TMIN_INTPRI=-15としている。（prc_kernel.h）
    　　変更する場合は、target_kernel.hでマクロTMIN_INTPRIを定義する。

    ・dis_intとena_intのサポートの有無，その制限事項
    　　サポートあり
    　　
    ・CFG_INTの制限事項と拡張（ターゲット定義で使用できる割込み属性）
    　　拡張なし
    　　
    ・カーネル管理外の割込みのサポートの有無，その設定方法
    　　未実装
    　　
    ・カーネル管理外の割込みに対してDEF_INH，CFG_INTをサポートするか
    　　未実装

    ・カーネル管理外の割込みを受け付けた場合の振舞い
    ・TOPPERS標準割込み処理モデルからの逸脱
    　　アーキテクチャ依存部のprc_user.txtを参照

(3-3) CPU例外処理に関する規定
    ・CPU例外ハンドラ番号の割当て
    　　プロセッサのベクタ番号をそのまま用いる。

    ・xsns_xpnの制限事項
  	　　アーキテクチャ依存部のprc_user.txtを参照

(3-4) 性能評価用システム時刻の参照に関する規定
    ・get_utmのサポートの有無，その制限事項
    　　サポートあり

(3-5) その他
    ・その他の制限事項

○ROM化の際の動作確認は行っていない。

○ROM化の際にdataセクションのアドレスマッピングが不正になる問題
　・dataセクションのサイズが0の場合、bssセクションが
　　textセクションの直後に配置されてしまう。
　　（処理系の不具合？）
　・そこでダミー変数を用意して、dataセクションのサイズが0にならない
　　ようにして回避している。
　・ダミー変数がコンパイラの最適化で削除されるのを防ぐため、
　　アセンブラソースファイル（target_support.S）内にダミー変数_dummyを
　　記述している。

    ・その他の拡張機能

○カーネル終了時の処理

ext_ker が呼び出されカーネル終了時には，apsh2ad.h にある apsh2ad_exit()
が実行される．ディフォルトでは，無限ループが書かれている．独自の処理処
理を追加したい場合は，apsh2ad_exit() の内容を書き換えること．

○各種設定の変更

幾つかのパラメータは変更可能になっている．設定ファイル毎に設定可能項目
は次のようになっている．
Makefile.targetを編集した場合は、make realcleanとmake dependを再度、
実行すること。

●Makefile.target の設定項目
　　メモリマップに関する設定項目は「(4) メモリマップ」を参照。

・GCC_TARGET
  GCCの suffix を定義

・INCLUDES
  インクルード指定

・COPTS
  Cコンパイラへのオプション

・LDFLAGS
  リンカへのオプション

●target_config.h の設定項目

・DEFAULT_PRC<プロセッサID>_ISTKSZ
  ディフォルトの非タスクコンテキスト用のスタックサイズ

・DEFAULT_PRC<プロセッサID>_ISTK_BOTTOM
  ディフォルトの非タスクコンテキスト用のスタックの終了番地
  　・非タスクコンテキスト用スタックポインタの初期値（底）

・SIL_DLY_TIM1/2
  微少時間待ちのための定義


●target_syssvc.h の設定項目

・BPS_SETTING
  コンソールに使用するポート（SCIFのポート1）のボーレート

・SCIFX_BPS_SETTING
  コンソールに使用するポートのボーレート

・SCIFX_CKE_SETTING
  コンソールに使用するポートのクロックソース

(3-6) タイマアーキテクチャ
  ・グローバルタイマ方式

(3-7) OS内部のロックアーキテクチャ
  ・ジャイアントロック方式

(3-8) スピンロックアーキテクチャ
  ・ネイティブ方式（最大 30個）

(3-9) メモリアーキテクチャ

(4) メモリマップ
    ・デフォルトのメモリマップ，その変更方法

(a) ROM化時（動作未確認）
　0x0000,0000 - 0x00ff,ffff FlashROM 16MB
　　　　　　　　　　　　　　　・vectorセクション
　　　　　　　　　　　　　　　・textセクション
　　　　　　　　　　　　　　　・vector_entryセクション
　0x3800,0000 - 0x39ff,ffff SRAM 32MB
　　　　　　　　　　　　　　　・dataセクション
　　　　　　　　　　　　　　　・bssセクション
　0xffd8,c000 - 0xffd8,ffff 内蔵RAM0 ページ3　8KB
　　　　　　　　　　　　　　　・非タスクコンテスト用スタック(CPU0)
　0xfffa,4000 - 0xfffa,7fff 内蔵RAM1 ページ1　8KB
　　　　　　　　　　　　　　　・非タスクコンテスト用スタック(CPU1)

(b) デバッグ時
　0x3800,0000 - 0x39ff,ffff SRAM 32MB
　　　　　　　　　　　　　　　・vectorセクション
　　　　　　　　　　　　　　　・textセクション
　　　　　　　　　　　　　　　・vector_entryセクション
　　　　　　　　　　　　　　　・dataセクション
　　　　　　　　　　　　　　　・bssセクション
　0xffd8,c000 - 0xffd8,ffff 内蔵RAM0 ページ3　8KB
　　　　　　　　　　　　　　　・非タスクコンテスト用スタック(CPU0)
　0xfffa,4000 - 0xfffa,7fff 内蔵RAM1 ページ1　8KB
　　　　　　　　　　　　　　　・非タスクコンテスト用スタック(CPU1)


●メモリマップに関するMakefile.target の設定項目

・ROM_BOOT
  ROM化する場合はtrueに定義する
  （動作未確認）

・TEXT_START_ADDRESS/DATA_START_ADDRESS
  テキストセクション，データセクションの開始アドレス

(5) タイマドライバ関連の情報
    ・タイムティックの周期，その変更方法
    　　デフォルトでは、1[msec]周期に定義されている。
	　　変更するには、マクロTIC_NUME、TIC_DENOの定義を変更すること。
	　　　定義場所：target_kernel.h
    　　
    ・使用するリソース（タイマ）
    　　CMT0を使用している。

(6) シリアルインタフェースドライバの情報
    ・使用するリソース（SIOコントローラ）
		コンソール出力には，SCIFのチャネル0を用いる．AP-SH2AD-0A上では
		SCIFのチャネル0は，CN5に接続されている．

  　　　　ポート数をマクロTNUM_PORT、TNUM_SIOPとして定義している。
 　　　　（デフォルトでは1）
	　　　定義場所：target_syssvc.h

		マクロTNUM_PORTの定義を2に変更することにより、SCIF1を有効にする
		こともできる。その場合、SCIF1はプロセッサのピン番号PE5,PE6
		（ボードのコネクタCN4の16,17端子）に接続されることを想定してい
		る。
 	　　マクロTNUM_PORTの定義を2に変更した場合、それぞれの出力先は
 	　　　・CPU0：SCIF0
 	　　　・CPU1：SCIF1
 	　　である。

	・通信パラメータ
	　（ボーレート，ビット数，パリティの有無，ストップビット）
	　　・ボーレート：38400bps
	　　　　定義場所：target_syssvc.hのマクロBPS_SETTING
	　　・ビット数：8ビット
	　　・パリティの有無：なし
	　　・ストップビット：1

(7) システムログ機能の情報
    ・システムログの低レベル出力の実現方法
 	　　デフォルトでは、SCIF0でポーリング出力を行っている。
 	　　マクロTNUM_PORTの定義を2に変更した場合、
 	　　　・CPU0：SCIF0
 	　　　・CPU1：SCIF1
 	　　でそれぞれポーリング出力を行っている。

	・注意事項
	　　ターゲットボードにシリアルポートのコネクタが１つしかないため、
	　　ターゲット依存部のデフォルトではシステムログ機能をグローバル方式
	　　で使用することを前提としている。（ソフトウェア自体はTNUM_PORTの
	　　定義を2に変更すれば、2ポート使用できるが、ハードウェア側もコネク
	　　タを追加する必要がある。）グローバル方式を指定するG_SYSLOGマクロ
	　　の定義は、アプリケーション側のMakefileで行う。サンプルプログラム
	　　では、G_SYSLOGマクロが定義されていないため、デフォルトのままビル
	　　ドするとmake depend時に以下のエラーメッセージが表示されるが、
	　　G_SYSLOGマクロの定義を追加することにより、正しくビルドできるよう
	　　になる。
	　　
	　　../fmp/target/apsh2ad_gcc/target_syssvc.h:131:2: error: 
	　　　#error !defined(G_SYSLOG) && (TNUM_PORT < TNUM_PRCID) :
	　　 　check G_SYSLOG and TNUM_PORT !
	　　
	　　G_SYSLOGの定義方法は、ユーザーズマニュアルfmp/user.txtの
	　　「6.1 Makefileの変数定義」
	　　「(M) システムログ機能のグローバル方式の指定」を参照

(8) システム構築手順
 　　doc/user.txtの「３．クイックスタートガイド」を参照。

(9) ターゲットシステム固有の注意事項
    ・タスクコンテキストに含まれるレジスタ，含まれないレジスタ
 	　　アーキテクチャ依存部のprc_user.txtを参照

(10) 類似のターゲットにポーティングする開発者向けの参考情報

APSH2ADターゲット依存部で使用するタイマやシリアルは，SH7205の内蔵機能の
みを使用するため，SH7205や近い構成のSH2A-DUALを用いた環境には容易にポーティ
ングが可能である．ポーティングに関しては，以下の初期化ルーチンにターゲ
ット毎の初期化を追加すればよい．

  ・target_initialize() : target_config.c
    ターゲット依存部の初期化（C言語）
  ・_hardware_init_hook : target_support.S
    低レベルのターゲット依存の初期化
    スタートアップモジュールの中で，メモリの初期化の前に呼び出される


(11) ディレクトリ構成・ファイル構成
 	target/apsh2ad_gcc/
		Makefile.target			Makefileのターゲット依存部
		target_stddef.h			t_stddef.hのターゲット依存部
		target_sil.h			sil.hのターゲット依存部
		target_kernel.h			kernel.hのターゲット依存部
		target_config.h			カーネル実装のターゲット依存部
		target_config.c			カーネル実装のターゲット依存部関連の
								定義
		target_support.S		カーネル実装のターゲット依存部関連の
								定義（アセンブリ言語）
		target_ipi.cfg			プロセッサ間割込みハンドラドライバの
								システムコンフィギュレーションファイル
		target_ipi.h			プロセッサ間割込みハンドラドライバの
								ヘッダファイル
								カーネルのみからインクルードされる．
		target_pcb.h			pcb.hのターゲット依存部
		target_rename.def		ターゲット依存部の内部識別名のリネーム
								定義
		target_rename.h			ターゲット依存部の内部識別名のリネーム
		target_unrename.h		ターゲット依存部の内部識別名のリネーム
								解除
		target_timer.cfg		タイマドライバのコンフィギュレーション
								ファイル
		target_timer.h			タイマドライバを使用するための定義
		apsh2ad.h				ターゲットのハードウェア資源の定義
		target.tf				kernel.tfのターゲット依存部
		target_check.tf			kernel_check.tfのターゲット依存部
		target_cfg1_out.h		cfg1_out.cのリンクに必要なスタブの定義
		target_syssvc.h			システムサービスのターゲット依存定義
		target_serial.cfg		serial.cfgのターゲット依存部
		target_serial.h			serial.hのターゲット依存部
		target_serial.c			serial.cのターゲット依存部の定義
		target_test.h			テストプログラムのターゲット依存定義
		target_user.txt			ターゲット依存部のユーザーズマニュアル
		MANIFEST				個別パッケージのファイルリスト
		E_PACKAGE				簡易パッケージのファイルリスト

(12) バージョン履歴

'10/05/10  Release1.1.1
　・APSH2AD(GCC)ターゲット依存部 最初のリリース

'09/04/03 
　・_hardware_init_hook
　　　低消費電力モードの設定をtarget_initialize()に移動
　　　（アセンブリ言語記述から、C言語記述へ）

'09/04/02 
　・target_support.S
　　　レジスタのアドレス定義をアーキテクチャ依存部に移動

'09/03/27 
　・target_timer.cfg
　　　prc_timer.cfgへのパス指定をasp/archからの相対パスに修正
　・コピーライトを追加

'09/03/24 
　・プロセッサ依存部Makefile.prcの呼び出しをコア依存部Makefile.sh2aに
　　変更

'08/05/19 
　・prc_initialize, prc_terminateのシンボルリネームを
　　アーキテクチャ依存部で行うよう変更


以下のRelease番号は追従したASPカーネルの番号を指す。

'08/05/13 Release1.3.1
　・シリアルポートの割込み番号のマクロ名をsh3,sh4と合わせた
　　　SCIFx_RXI_INTNO, SCIFx_TXI_INTNO (x=0, 1, 2, 3)

'08/05/12 Release1.3.0
　・非依存部の変更内容に対応
　・最適化対策に無限ループにvolatile変数を用いていたのを元に戻した。
　　（while(i)をwhile(1)に変更）
　　　　・target_config.cのtarget_exit(void)
　　　　・apsh2ad.hのapsh2ad_exit(void)
　　　　・target_stddef.hのTOPPERS_assert_abort(void)

'08/03/13 Release1.1.2
　・HEW版と同期
　・target_support.S
　　　バスステートコントローラの設定の誤りを修正
　・target_config.h
　　　SIL_DLY_TIM1、SIL_DLY_TIM2の値の見直し
　・test_log.txtを新設　　asp/test/test_dlynse.cの実行結果を記録

'08/02/05 Release1.1.1
　・ROM化の際にdataセクションのアドレスマッピングが不正になる問題に対応
　・シリアルドライバ
　　　ポート番号毎に個別に割込み禁止／許可するように修正
　・target_config.c
　　・target_exit()
　　　　while(1)の無限ループが最適化により削除される可能性があるので、
　　　　volatile修飾した変数にアクセスするように修正
