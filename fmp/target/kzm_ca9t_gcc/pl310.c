#include <kernel.h>
#include <t_syslog.h>
#include <t_stdlib.h>
#include <sil.h>
#include "syssvc/serial.h"
#include "syssvc/syslog.h"
#include "kernel_cfg.h"
#include "pl310.h"

static uint32_t pl310_way_mask;  /* Bitmask of active ways */

static inline void cache_wait_way(uint32_t *reg, unsigned long mask)
{
    /* wait for cache operation by line or way to complete */
    while (sil_rew_mem(reg) & mask);
}

static inline void cache_sync(void)
{
    //*((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_CACHE_SYNC)) = 0;
    sil_wrw_mem((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_CACHE_SYNC), 0);
}

static inline void pl310_inv_all(void)
{
    /* invalidate all ways */
    //*((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_INV_WAY)) = pl310_way_mask;
    sil_wrw_mem((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_INV_WAY), pl310_way_mask);
    cache_wait_way((uint32_t *)(MPCORE_L2CACHE_BASE+PL310_INV_WAY), pl310_way_mask);
    cache_sync();
}

void pl310_init(uint32_t aux_val, uint32_t aux_mask)
{
    uint32_t aux;
    uint32_t cache_id;
    int ways;

    cache_id = sil_rew_mem((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_CACHE_ID));
    aux = sil_rew_mem((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_AUX_CTRL));

    aux &= aux_mask;
    aux |= aux_val;

    if (aux & (1 << 16))
        ways = 16;
    else
        ways = 8;

    pl310_way_mask = (1 << ways) - 1;
    
    /* pl310 controller is disabled */
    //*((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_AUX_CTRL)) = aux;
    sil_wrw_mem((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_AUX_CTRL), aux);
    
    pl310_inv_all();
    
    /* enable PL310 */
    //*((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_CTRL)) = 1;
    sil_wrw_mem((uint32_t *)(MPCORE_L2CACHE_BASE + PL310_CTRL), 1);
    
}
