#ifndef TOPPERS_TARGET_ASM_INC
#define TOPPERS_TARGET_ASM_INC

/*
 *  チップ依存モジュール（MPCORE用）
 */
#include "arm_gcc/mpcore/chip_asm.inc"

#endif /* TOPPERS_TARGET_ASM_INC */
